@extends('layouts.master')

@section('title', 'ตั้งค่าผู้ใช้งาน')
@section('css')
  <style type="">
    
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-8">
      <h2 class="form-inline">
        รายการเอกสาร
      </h2>
    </div>

    <div class="col-md-4 text-right">
      <a href="#" class="btn btn-lg btn-success" data-toggle="modal" data-target=".document-section-1">
        <i class="fa fa-plus"></i> เพิ่มข้อมูล
      </a>
    </div>

    {{-- @php
      $arr_ = [1,2,4,5];
      echo (string)in_array("3",$arr_);
    @endphp
    @for ($i=1; $i<6; $i++)
      <div class="icheck-square col-12">
        <input type="checkbox" name="chkIndicator[]" value="{{ $i }}" @if((string)in_array($i, $arr_)) {{'checked'}} @endif>
        <label>
          {{ 'detail : '.$i }}
        </label>
      </div>
      <br>
    @endfor --}}

  </div>

  <div class="row"> 
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table class="tbl-indicator table-bordered tbl-document">
            <thead class="text-center">
              <tr>
                <th width="5%">ลำดับ</th>
                <th width="15%">ชื่อผู้ใช้งาน</th>
                <th width="%">ชื่อ-นามสกุล</th>
                <th width="25%">แก้ไข</th>
              </tr>
            </thead>  
            <tbody>
              {{-- @if(count($evaluators) > 0)
                @foreach ($evaluators as $i=>$evaluator)
                  <tr class="bg-secondary">
                    <td class="text-center">{{$i+1}}</td>
                    
                    <td>
                        {{$evaluator->username}}
                    </td>

                    <td>
                        {{$evaluator->name}}
                    </td>
                    <td>
                      {{$evaluator->office_name}}
                    </td>
                    <td>
                      {{ $evaluator->user_group_name }}
                    </td>
                    <td class="text-center"> 
                      <a href="{{ url('setting/'.$evaluator->id.'/evaluator-edit') }}" class="btn btn-icons btn-warning" title="แก้ไข">
                        <i class="fa fa-edit"></i>
                      </a>

                      <button type="button" onclick="deleteEvaluator({{ $evaluator->id }})" class="btn btn-icons btn-danger" title="ลบข้อมูล">
                        <i class="fa fa-trash-alt"></i>
                      </button>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <th colspan="6" class="text-center">ยังไม่มีข้อมูล</th>
                </tr>
              @endif --}}
            </tbody>
          </table> 
          
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade document-section-1">
    <div class="modal-dialog modal-" style="max-width:80%;">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">
            <i class="mdi mdi-file-check"></i> 
            การตรวจสอบความถูกต้องของการผลิต (1)
          </h3>
    
          <button class="btn btn-icons btn-rounded btn-outline-danger" data-dismiss="modal">
          <i class="fa fa-times"></i>
          </button>
        </div>

        <form id="frmDocumentSection1">
          <div class="modal-body pt-1">
            <div class="row">

              <div class="col-md-12">
                <table class="tbl-">
                  <tr>
                    <th style="width:50%;">
                      <div class="form-inline">
                        <span class="mr-2">ชื่อผลิตภัณฑ์</span>
                        <input name="txtProductName" class="form-control form-control-sm">
                      </div>
                    </th>

                    <th style="width:50%;">
                      วันที่ {{ th_date_short(date('Y-m-d')) }}
                    </th>
                  </tr>

                  <tr>
                    <td colspan="2" style="vertical-align: top;">
                      <div class="row">
                        <div class="col-md-12">
                          <u>การตรวจสอบ RAW MATERIAL จาก WHEREHOUSE</u>

                          <div class="icheck-square">
                            <input type="radio" name="rdoTypeMilk" value="ใช้นมชนิดเดิมผลิตต่อ" checked>
                            <label>ใช้นมชนิดเดิมผลิตต่อ</label>
                          </div>

                          <div class="icheck-square">
                            <input type="radio" name="rdoTypeMilk" value="เคลียร์นมชนิดก่อนหน้าออกเรียบร้อย">
                            <label>เคลียร์นมชนิดก่อนหน้าออกเรียบร้อย</label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <u>ชนิดของนมที่ส่ง </u>
                          
                          <div class="form-inline mt-1">
                            <span class="mr-2">1.</span>
                            <input name="txtTypeMilk1" class="form-control form-control-sm">
                          </div>
                          
                          <div class="form-inline mt-1">
                            <span class="mr-2">2.</span>
                            <input name="txtTypeMilk2" class="form-control form-control-sm">
                          </div>
                          
                          <div class="form-inline mt-1">
                            <span class="mr-2">3.</span>
                            <input name="txtTypeMilk3" class="form-control form-control-sm">
                          </div>
                          
                          <div class="form-inline mt-1">
                            <span class="mr-2">4.</span>
                            <input name="txtTypeMilk4" class="form-control form-control-sm">
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" onclick="" class="btn btn-lg btn-success">
              <i class="far fa-save"></i>บันทึก & เปลี่ยนแปลง
            </button>
          </div>
        </form>
  
      </div>
    </div>
  </div>

  <div class="modal fade document-section-2">
    <div class="modal-dialog modal-lg" style="max-width:%;">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">
            <i class="mdi mdi-file-check"></i> 
            การตรวจสอบความถูกต้องของการผลิต (2)
          </h3>
    
          <button class="btn btn-icons btn-rounded btn-outline-danger" data-dismiss="modal">
          <i class="fa fa-times"></i>
          </button>
        </div>

        <form id="frmDocumentSection2">
          <div class="modal-body pt-1">
            <div class="row">
              <div class="col-md-12">
                <table class="tbl-">
                  <tr>

                    <td style="vertical-align: top;">
                      <div class="row">
                        <div class="col-md-12 mb-1">
                          <u>Process Flow (รายละเอียดการผลิต)</u>
                        </div>

                        <div class="col-md-6">
                          <span class="bg-success">
                            เบลนเดอร์ที่ใช้
                          </span>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="เบลนเดอร์ 1 (Prebiotic)">
                            <label>เบลนเดอร์ 1 (Prebiotic)</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="เบลนเดอร์ 2 (Prebiotic)">
                            <label>เบลนเดอร์ 2 (Prebiotic)</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="เบลนเดอร์ 1 และ สไมล์(Synbiotic)">
                            <label>เบลนเดอร์ 1 และ สไมล์(Synbiotic)</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="ไอเอฟ เซกรีแคร์ (IF Segrecare)">
                            <label>ไอเอฟ เซกรีแคร์ (IF Segrecare)</label>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <span class="bg-success">
                            เครื่องบรรจุที่ใช้
                          </span>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="Filling line 1">
                            <label>Filling line 1</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="Filling line 2">
                            <label>Filling line 2</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="Filling line 3">
                            <label>Filling line 3</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="Filling line 4">
                            <label>Filling line 4</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="Filling line 5">
                            <label>Filling line 5</label>
                          </div>
                          
                          <div class="icheck-square">
                            <input type="checkbox" name="chkBlender[]" value="Filling line 6">
                            <label>Filling line 6</label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          บันทึกโดย
                          {{ '..................................................' }}
                          (พนักงานเทนม)
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" onclick="" class="btn btn-lg btn-success">
              <i class="far fa-save"></i>บันทึก & เปลี่ยนแปลง
            </button>
          </div>
        </form>
  
      </div>
    </div>
  </div>

  <div class="modal fade document-section-3">
    <div class="modal-dialog modal-lg" style="max-width:%;">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">
            <i class="mdi mdi-file-check"></i> 
            การตรวจสอบความถูกต้องของการผลิต (3)
          </h3>
    
          <button class="btn btn-icons btn-rounded btn-outline-danger" data-dismiss="modal">
          <i class="fa fa-times"></i>
          </button>
        </div>

        <form id="frmDocumentSection3">
          <div class="modal-body pt-1">
            <div class="row">
              <div class="col-md-12">
                
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" onclick="" class="btn btn-lg btn-success">
              <i class="far fa-save"></i>บันทึก & เปลี่ยนแปลง
            </button>
          </div>
        </form>
  
      </div>
    </div>
  </div>

  <div class="modal fade document-section-4">
    <div class="modal-dialog modal-lg" style="max-width:%;">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">
            <i class="mdi mdi-file-check"></i> 
            การตรวจสอบความถูกต้องของการผลิต (4)
          </h3>
    
          <button class="btn btn-icons btn-rounded btn-outline-danger" data-dismiss="modal">
          <i class="fa fa-times"></i>
          </button>
        </div>

        <form id="frmDocumentSection4">
          <div class="modal-body pt-1">
            <div class="row">
              <div class="col-md-12">
                
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" onclick="" class="btn btn-lg btn-success">
              <i class="far fa-save"></i>บันทึก & เปลี่ยนแปลง
            </button>
          </div>
        </form>
  
      </div>
    </div>
  </div>
@endsection

@section('js')
<script type="text/javascript">  
  $(function () {
    // var getEvaluator = function(){
    datatable_document = $('.tbl-document').DataTable({
      processing: true,
      serverSide: true,
      ordering: false,
      // order: [[ 1, 'asc' ]],
      iDisplayLength: 10,
      aLengthMenu: [
        [10, 20, 50, -1], 
        [10, 20, 50, "ทั้งหมด"]
      ], 
      language: {
        search: "" 
      },

      ajax: {
        type: "GET",
        url: "{{url('get-document-list')}}",
        data: function (d) {
          // d.year = $('select[name=ddlYear]').val();
          // d.round = $('select[name=ddlRound]').val();
          // console.log(d);
        },
      },
      columns: [
        { data: 'id'},
        { data: 'username' },
        { data: 'name' },
        { data: 'id' },
      ],
      columnDefs: [
        { "targets": 0, "className":"text-center",
          render: function (data, type, row, meta) {
            // console.log(meta);
            return (meta.row + meta.settings._iDisplayStart + 1);
          } 
        },
        {
          targets: -1,
          searchable: false,
          className: 'text-center',

          render: function (data, type, row, meta) {
            return `<a href="#" onclick="getDocument('${1}')" class="btn btn-icons btn-secondary" title="ส่วนที่ 1">\
                      <i class="mdi mdi-numeric-1-box m-0"></i>\
                    </a>\
                    <a href="#" onclick="getDocument(2)" class="btn btn-icons btn-warning" title="ส่วนที่ 2">\
                      <i class="mdi mdi-numeric-2-box m-0"></i>\
                    </a>\
                    <a href="#" onclick="getDocument(2)" class="btn btn-icons btn-primary" title="ส่วนที่ 3">\
                      <i class="mdi mdi-numeric-3-box-multiple-outline m-0"></i>\
                    </a>\
                    <a href="#" onclick="getDocument(2)" class="btn btn-icons btn-success" title="ส่วนที่ 4">\
                      <i class="mdi mdi-numeric-4-box m-0"></i>\
                    </a>\
                    <button onclick="deleteDocument('${data}')" class="btn btn-icons btn-danger" title="ลบข้อมูล">\
                    <i class="fa fa-trash-alt"></i></button>`;
          }
        }
      ],
    });
    
    getDocument = function(section) { 
      $('.document-section-'+section).modal('show');
      // $.ajax({
      //   type: "POST",
      //   url: `{{ url('setting/${user_id}/user-delete') }}`,
      //   // data: {user_id: id},
        
      //   success: function (response) {
      //     console.log(response);
      //     alert(response.msg)
      //     datatable_evaluator.ajax.reload();
      //     // location.reload();
      //   },
      //   error: function (err) {
      //     console.log(err.responseJSON);
      //   }
      // });
    }

    deleteDocument = function(id) { 
      if(confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
        // $.ajax({
        //   type: "POST",
        //   url: `{{ url('setting/${id}/user-delete') }}`,
        //   // data: {user_id: id},
          
        //   success: function (response) {
        //     console.log(response);
        //     alert(response.msg)
        //     datatable_evaluator.ajax.reload();
        //     // location.reload();
        //   },
        //   error: function (err) {
        //     console.log(err.responseJSON);
        //   }
        // });
      }
    }
  });

  var resident_main = (function () {
    var datatable_resident1;

    var datatable_resident = function () {
      datatable_resident1 = $('.tbl-resident').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        // order: [[ 1, 'asc' ]],
        iDisplayLength: 20,
        aLengthMenu: [
          [20, 30, 40, 50, -1], 
          [20, 30, 40, 50, "ทั้งหมด"]
        ], 
        language: {
          search: "" 
        },
  
        ajax: {
          type: "GET",
          url: "{{ url('setting/get-resident') }}",
          data: function (d) {
            // d.year = $('select[name=ddlYear]').val();
            // d.round = $('select[name=ddlRound]').val();
            // console.log(d);
          },
        },
        columns: [
          { data: 'id'},
          { data: 'name' },
          { data: 'address' },
          { data: 'card_id' },
          { data: 'tel' },
          { data: 'id' },
        ],
        columnDefs: [
          { 
            targets: 0, "className":"text-center",
            render: function (data, type, row, meta) {
              // console.log(meta);
              return (meta.row + meta.settings._iDisplayStart + 1);
            } 
          },

          {
            targets: -1,
            searchable: false,
            className: 'text-center',
  
            render: function (data, type, row, meta) { //`
              return '<a class="btn btn-icons btn-warning" href="{{ url("setting") }}'+'/'+data+'/resident-edit" title="ดู/แก้ไขข้อมูล"><i class="fa fa-edit"></i></a>\
                <button class="btn btn-icons btn-danger" onclick="deleteResident('+data+')" title="ลบข้อมูล"><i class="fa fa-trash-alt"></i></button>';
            }
          }
        ],
      });
    }
  
    deleteResident = function(id) { 
      if(confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
        $.ajax({
          type: "POST",
          url: `{{ url('setting/${id}/resident-delete') }}`,
          // data: {user_id: id},
          
          success: function (res) {
            console.log(res);
            alert(res.msg)
            datatable_resident1.ajax.reload();
            // location.reload();
          },
          error: function (err) {
            console.log(err.responseJSON);
          }
        });
      }
    }

    return {
      init: function () {
        datatable_resident();
      }
    }

  })()

  $(document).ready(function () {
    resident_main.init();
  });
</script>
@endsection