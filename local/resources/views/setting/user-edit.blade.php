@extends('layouts.master')

@section('title', 'แก้ไขข้อมูลผู้ใช้งาน')

@section('css')

@stop

@section('content')
  <div class="row">
    <div class="col-md-12">
      <h2 class="form-inline">
        แก้ไขข้อมูลผู้ใช้งาน
      </h2>
    </div>
  </div>

  <div class="row"> 
    <div class="col-md-12">

      <form id="frmInsert"> 
        @csrf
        <div class="card">
          <div class="card-body pb-3">
            <div class="row">
              <div class="col-md-4 form-group">
                <label>Username</label>
                <input type="text" name="txtUsername" value="{{ $user->username }}" onfocusout="Validate(this.name, this.tagName)" class="form-control txtUsername">
              </div>

              <div class="col-md-4 form-group">
                <label>Password</label>
                <button type="button" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target=".modal-reset-password">
                  <i class="fa fa-key"></i>
                  รีเซ็ตรหัสผ่าน
                </button>
              </div>

              <div class="col-md-4 form-group">
                <label>สถานะ</label>
                <select name="ddlGroup" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlGroup">
                  <option value="" disabled selected>สถานะ</option>
                  
                  {{-- @foreach ($user_group as $group)
                    <option value="{{ $group->id }}" {{ $user->user_group_id==$group->id ?'selected' :'' }}>
                      {{ $group->user_group_name }}
                    </option>
                  @endforeach --}}
                </select>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4 form-group">
                <label>ชื่อ-นามสกุล</label>
                <input type="text" name="txtName" value="{{ $user->name }}" onfocusout="Validate(this.name, this.tagName)" class="form-control txtName">
              </div>

              <div class="col-md-4 form-group">
                <label>หน่วยงาน</label>
                <select name="ddlOffice" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlOffice">
                  <option value="" disabled selected>เลือกหน่วยงาน</option>
                  {{-- @foreach ($offices as $office)
                    <option value="{{ $office->office_id }}" office_type="{{ $office->office_type_id }}" {{ $office->office_type_id!=$user->office_type_id ?'style=display:none' :'' }} {{ $user->office_id==$office->office_id ?'selected' :'' }}>
                      {{ $office->office_name }}
                    </option>
                  @endforeach --}}
                </select>
              </div>

            </div>

            <ul class="nav nav-tabs tab-basic" {{ $user->user_group_id!=2 && $user->user_group_id!=3 ?'' :'style=display:none' }} role="tablist">
              {{-- @foreach ($office_type_view as $type)
                <li class="nav-item">
                  <a class="nav-link {{$type->office_type_id==2 ?'active' :''}}" id="tab-head{{$type->office_type_id}}" data-toggle="tab" href="#tab-type{{$type->office_type_id}}" role="tab" aria-controls="tab-type{{$type->office_type_id}}" aria-selected="{{$type->office_type_id==2 ?'true' :'false'}}">
                    ตัวชี้วัด{{$type->office_type_name}}
                  </a>
                </li>
              @endforeach --}}
            </ul>
              
            {{-- <div class="tab-content tab-content-basic" {{ $user->user_group_id!=2 && $user->user_group_id!=3 ?'' :'style=display:none' }}>
              @foreach ($office_type_view as $type)
                <div class="tab-pane fade {{$type->office_type_id==2 ?'show active' :''}}" id="tab-type{{$type->office_type_id}}" role="tabpanel" aria-labelledby="tab-head{{$type->office_type_id}}">
                  @php
                    $indicators = DB::table('indicator')
                      ->where([
                        'type_id'=>$type->office_type_id,
                        'ind_year'=>$year, 
                        'ind_round'=>$round
                      ])->get(); 
                  @endphp

                  @if(count($indicators) > 0)
                    @foreach ($indicators as $i=>$ind)
                      <li class="list-group-item">
                      <div class="custom-control custom-checkbox{{ $ind->num_02=='#' ?' pl-0' :'' }}">
                          @if ($ind->num_02 == '#')
                            <label class="col-form-label py-0" for="chkIndicator{{ $ind->ind_id }}">
                              {{ $ind->ind_detail }}
                            </label>
                          @else

                            @php
                              $arr_evaluate_group = explode(',',$user->evaluate_group);
                            @endphp
                            <input type="checkbox" id="chkIndicator{{ $ind->ind_id }}" name="chkIndicator[]" value="{{ $ind->ind_id }}"  class="custom-control-input" {{ ((string)in_array($ind->ind_id, $arr_evaluate_group)) ?'checked' :'' }}>
                          
                            <label class="custom-control-label" for="chkIndicator{{ $ind->ind_id }}">
                              {{ $ind->ind_detail }}
                            </label>
                          @endif
                        </div>
                      </li>
                    @endforeach
                  @else
                    <li class="list-group-item">
                      <b>ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</b>
                    </li>
                  @endif
                </div>
              @endforeach
            </div> --}}

          </div>

          <div class="card-footer text-center">
            {{-- <input type="hidden" name="round" value="{{ $round }}"> --}}
            <button type="button" onclick="updateUser({{$user->id}})" class="btn btn-lg btn-warning btnSave">
              <i class="far fa-edit"></i> บันทึก & เปลี่ยนแปลง
            </button>

            <button type="reset" class="btn btn-lg btn-danger">
              <i class="fa fa-undo"></i> ยกเลิก
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
  
  <div class="modal fade modal-reset-password">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">
            <i class="fa fa-key"></i> 
            รีเซ็ตรหัสผ่าน
          </h3>
    
          <button class="btn btn-icons btn-rounded btn-outline-danger" data-dismiss="modal">
          <i class="fa fa-times"></i>
          </button>
        </div>
  
        <div class="modal-body pb-2 pt-1">
          <div class="row">
            <div class="col-md-12 form-group">
              <label>รหัสผ่านใหม่</label>
              <input type="text" name="txtResetPassword" class="form-control txtResetPassword" placeholder="รหัสผ่านใหม่">
            </div>
          </div>
        </div>

        <div class="modal-footer text-right">
          <button type="button" onclick="ResetPassword({{ $user->id }})" class="btn btn-lg btn-success">
            <i class="far fa-save"></i>
            ยืนยัน
          </button>
        </div>
  
      </div>
    </div>
  </div>
@endsection

@section('js')
<script type="text/javascript"> 
  $('select[name="ddlGroup"]').change(function(e){
    // console.log(e.target.value);
    var group = $(this).val();
    var select_office = $('select[name="ddlOffice"] > option').not(':first');

    if(group==1 || group==4 || group==5 || group==6) {
      $('.tab-basic, .tab-content-basic').show();
    } else {
      $('.tab-basic, .tab-content-basic').hide();
    }
    
    $.each(select_office, function (i, val) {
      var office_type = select_office.eq(i).attr('office_type');
      console.log($(this).val());
      if(group==1 || group==4 || group==5 || group==6) {
        if( (office_type == 1) ) {
          select_office.eq(i).show();
        } else {
          select_office.eq(i).hide();
        }
      }

      else if(group==2 || group==3) {
        if( (office_type != 1) ) {
          select_office.eq(i).show();
        } else {
          select_office.eq(i).hide();
        }
      }

    });
    $('select[name="ddlOffice"] > option:first').prop('selected',true);
  });

  function updateUser(user_id) { 
    $.ajax({
      type: "POST",
      url: `{{ url('setting/${user_id}/user-update') }}`,
      data: $('#frmInsert').serialize(),
      
      success: function (response) {
        console.log(response);
        alert(response.msg)
        if(response.validate > 0){
          $('.txtUsername').focus();
        }
      },
      error: function (err) {
        console.log(err.responseJSON);
      }
    });
  }

  function ResetPassword(user_id){
    if(confirm('ต้องการรีเซ็ตรหัสผ่านหรือไม่ ?')){
      if($('.txtResetPassword').val()=='') {
        alert('กรุณากรอกรหัสผ่าน');
        $('.txtResetPassword').focus();
      } else {
        $.ajax({
          type: "POST",
          url: `{{ url('setting/${user_id}/reset-password') }}`,
          data: { txtResetPassword:$('.txtResetPassword').val()},
          
          success: function (response) {
            console.log(response);
            alert(response.msg);
            $('.modal-reset-password').modal('hide')
          },
          error: function (err) {
            console.log(err.responseJSON);
          }
        });
      }
    }
  }

  function Validate(name, tagname){
    if(tagname == 'SELECT') {
      if($('select[name="'+name+'"]').val() == null) { // || $('select[name="'+name+'"]').val() == 0
        $('select[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('select[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    } 
    else if(tagname == 'INPUT') {
      if($('input[name="'+name+'"]').val() == '') {
        $('input[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('input[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    }
  }

  function ValidateClick(){ 
    if($('.txtUsername').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtUsername').css('border-color','red');
    }
    if($('.txtPassword').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtPassword').css('border-color','red');
    }
    if($('.txtName').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtName').css('border-color','red');
    }

    if($('.ddlGroup').val()==null){
      $('.btnSave').attr('disabled',true);
      $('.ddlGroup').css('border-color','red');
    }
    if($('.ddlOffice').val()==null){
      $('.btnSave').attr('disabled',true);
      $('.ddlOffice').css('border-color','red');
    }
  }
</script>
@endsection