@extends('layouts.master')

@section('title', 'ตั้งค่าผู้ใช้งาน')
@section('css')
  <style type="">
    
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-8">
      <h2 class="form-inline">
        ตั้งค่าผู้ใช้งาน
      </h2>
    </div>

    <div class="col-md-4 text-right">
      <a href="{{ url('setting/user-create') }}" class="btn btn-lg btn-success">
        <i class="fa fa-plus"></i> เพิ่มข้อมูล
      </a>
    </div>

    {{-- @php
      $arr_ = [1,2,4,5];
      echo (string)in_array("3",$arr_);
    @endphp
    @for ($i=1; $i<6; $i++)
      <div class="icheck-square col-12">
        <input type="checkbox" name="chkIndicator[]" value="{{ $i }}" @if((string)in_array($i, $arr_)) {{'checked'}} @endif>
        <label>
          {{ 'detail : '.$i }}
        </label>
      </div>
      <br>
    @endfor --}}

  </div>

  <div class="row"> 
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table class="tbl-indicator table-bordered tbl-user">
            <thead class="text-center">
              <tr>
                <th width="5%">ลำดับ</th>
                <th width="15%">ชื่อผู้ใช้งาน</th>
                <th width="%">ชื่อ-นามสกุล</th>
                <th width="%">หน่วยงาน</th>
                <th width="15%">สถานะ</th>
                <th width="10%">แก้ไข</th>
              </tr>
            </thead>  
            <tbody>
              {{-- @if(count($evaluators) > 0)
                @foreach ($evaluators as $i=>$evaluator)
                  <tr class="bg-secondary">
                    <td class="text-center">{{$i+1}}</td>
                    
                    <td>
                        {{$evaluator->username}}
                    </td>

                    <td>
                        {{$evaluator->name}}
                    </td>
                    <td>
                      {{$evaluator->office_name}}
                    </td>
                    <td>
                      {{ $evaluator->user_group_name }}
                    </td>
                    <td class="text-center"> 
                      <a href="{{ url('setting/'.$evaluator->id.'/evaluator-edit') }}" class="btn btn-icons btn-warning" title="แก้ไข">
                        <i class="fa fa-edit"></i>
                      </a>

                      <button type="button" onclick="deleteEvaluator({{ $evaluator->id }})" class="btn btn-icons btn-danger" title="ลบข้อมูล">
                        <i class="fa fa-trash-alt"></i>
                      </button>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <th colspan="6" class="text-center">ยังไม่มีข้อมูล</th>
                </tr>
              @endif --}}
            </tbody>
          </table> 
          
        </div>
      </div>
    </div>
  </div>
  {{-- <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
            <div class="accordion basic-accordion" id="div-type-office" role="tablist">
              <div class="row">
                @foreach ($office_type as $type)
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header" role="tab" id="heading{{$type->office_type_id}}">
                        <h6 class="mb-0">
                          <a data-toggle="collapse" href="#div-type{{$type->office_type_id}}" aria-expanded="false" aria-labelledby="heading{{$type->office_type_id}}" aria-controls="div-type{{$type->office_type_id}}">
                            <i class="card-icon fa fa-home"></i><u>ตัวชี้วัด{{$type->Name_office_type}}</u>
                          </a>
                        </h6>
                      </div>
                      <div id="div-type{{$type->office_type_id}}" class="collapse" role="tabpanel" data-parent="#div-type-office">
                        <div class="card-body">
                          <ul class="list-group">
                            @php
                              $indicators = App\Indicator::where(['type_id'=>$type->office_type_id,'ind_year'=>$year])->get();
                            @endphp
  
                            @if(count($indicators) > 0) 
                              @foreach ($indicators as $i => $ind)
                                <li class="list-group-item">
                                  <a href = "{{ url('evaluation').'/'.$ind->ind_id.'/3' }}"> {{$ind->ind_detail}}</a>
                                </li>
                              @endforeach
                            @else
                              <li class="list-group-item">
                                <a>ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}}</a>
                              </li>
                            @endif
                        </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </ul>
        </div>
      </div>
    </div>
  </div> --}}
@endsection

@section('js')
<script type="text/javascript">  
  $(function () {
    // var getEvaluator = function(){
    datatable_evaluator = $('.tbl-user').DataTable({
      processing: true,
      serverSide: true,
      ordering: false,
      // order: [[ 1, 'asc' ]],
      iDisplayLength: 10,
      aLengthMenu: [
        [10, 20, 50, -1], 
        [10, 20, 50, "ทั้งหมด"]
      ], 
      language: {
        search: "" 
      },

      ajax: {
        type: "GET",
        url: "{{url('setting/get-user')}}",
        data: function (d) {
          // d.year = $('select[name=ddlYear]').val();
          // d.round = $('select[name=ddlRound]').val();
          // console.log(d);
        },
      },
      columns: [
        { data: 'id'},
        { data: 'username' },
        { data: 'name' },
        { data: 'office_name' },
        { data: 'user_group_name' },
        { data: 'id' },
      ],
      columnDefs: [
        { "targets": 0, "className":"text-center",
          render: function (data, type, row, meta) {
            // console.log(meta);
            return (meta.row + meta.settings._iDisplayStart + 1);
          } 
        },
        {
          targets: -1,
          searchable: false,
          className: 'text-center',

          render: function (data, type, row, meta) { //`
            return '<a class="btn btn-icons btn-warning" href="{{ url("setting") }}'+'/'+data+'/user-edit" title="แก้ไขข้อมูล">\
              <i class="fa fa-edit m-0"></i></a>\
              <button class="btn btn-icons btn-danger" onclick="deleteUser('+data+')" title="ลบข้อมูล"><i class="fa fa-trash-alt"></i></button>';
          }
        }
      ],
    });
    // getEvaluator();
  
    deleteUser = function(user_id) { 
      if(confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
        $.ajax({
          type: "POST",
          url: `{{ url('setting/${user_id}/user-delete') }}`,
          // data: {user_id: id},
          
          success: function (response) {
            console.log(response);
            alert(response.msg)
            datatable_evaluator.ajax.reload();
            // location.reload();
          },
          error: function (err) {
            console.log(err.responseJSON);
          }
        });
      }
    }
  });
</script>
@endsection