@extends('layouts.master')

@section('title', 'เพิ่มข้อมูลผู้ใช้งาน')
@section('css')
  <style media="screen">
    .btnDeleteChoice{
      cursor: pointer;
    }
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-12">
      <h2 class="form-inline">
        เพิ่มข้อมูลผู้ใช้งาน
      </h2>
    </div>
    {{-- @php
      $pass = 'TH7000005868';
      $hash = bcrypt($pass);
    @endphp

    @if (password_verify($pass, $hash)) 
      {{'ถูก : '.$pass }} = {{ $hash }} <br>
    @else 
      {{'ผิด'}} <br>
    @endif --}}
  </div>

  <div class="row"> 
    <div class="col-md-12">

      <form id="frmInsert"> 
        @csrf
        <div class="card">
          <div class="card-body pb-3">
            <div class="row">
              <div class="col-md-4 form-group">
                <label>Username</label>
                <input type="text" name="txtUsername" onfocusout="Validate(this.name, this.tagName)" class="form-control txtUsername">
              </div>

              <div class="col-md-4 form-group">
                <label>Password</label>
                <input type="password" name="txtPassword" onfocusout="Validate(this.name, this.tagName)" class="form-control txtPassword">
              </div>

              <div class="col-md-4 form-group">
                <label>สถานะ</label>
                <select name="ddlGroup" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlGroup">
                  <option value="" disabled selected>สถานะ</option>
                  {{-- @foreach ($user_group as $group)
                    <option value="{{ $group->id }}">
                      {{ $group->user_group_name }}
                    </option>
                  @endforeach --}}
                </select>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4 form-group">
                <label>ชื่อ-นามสกุล</label>
                <input type="text" name="txtName" onfocusout="Validate(this.name, this.tagName)" class="form-control txtName">
              </div>

              <div class="col-md-4 form-group">
                <label>ตำแหน่ง</label>
                <input type="text" name="txtPosition" class="form-control txtPosition">
              </div>

              <div class="col-md-4 form-group">
                <label>หน่วยงาน</label>
                <select name="ddlOffice" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlOffice">
                  <option value="" disabled selected>เลือกหน่วยงาน</option>
                  {{-- @foreach ($offices as $office)
                    <option value="{{ $office->office_id }}" office_type="{{ $office->office_type_id }}">
                      {{ $office->office_name }}
                    </option>
                  @endforeach --}}
                </select>
              </div>

            </div>

            
            {{-- <ul class="nav nav-tabs tab-basic" role="tablist">
              @foreach ($office_type_view as $type)
                <li class="nav-item">
                  <a class="nav-link {{$type->office_type_id==2 ?'active' :''}}" id="tab-head{{$type->office_type_id}}" data-toggle="tab" href="#tab-type{{$type->office_type_id}}" role="tab" aria-controls="tab-type{{$type->office_type_id}}" aria-selected="{{$type->office_type_id==2 ?'true' :'false'}}">
                    ตัวชี้วัด{{$type->office_type_name}}
                  </a>
                </li>
              @endforeach
            </ul>
            
            <div class="tab-content tab-content-basic">
              @foreach ($office_type_view as $type)
                <div class="tab-pane fade {{$type->office_type_id==2 ?'show active' :''}}" id="tab-type{{$type->office_type_id}}" role="tabpanel" aria-labelledby="tab-head{{$type->office_type_id}}">
                  @php
                    $indicators = DB::table('indicator')
                      ->where([
                        'type_id'=>$type->office_type_id,
                        'ind_year'=>$year, 
                        'ind_round'=>$round
                      ])->get(); 
                  @endphp

                  @if(count($indicators) > 0)
                    @foreach ($indicators as $i=>$ind)
                      <li class="list-group-item">
                      <div class="custom-control custom-checkbox{{ $ind->num_02=='#' ?' pl-0' :'' }}">
                          @if ($ind->num_02 == '#')
                            <label class="col-form-label py-0" for="chkIndicator{{ $ind->ind_id }}">
                              {{ $ind->ind_detail }}
                            </label>
                          @else
                            <input type="checkbox" id="chkIndicator{{ $ind->ind_id }}" name="chkIndicator[]" value="{{ $ind->ind_id }}"  class="custom-control-input">
                          
                            <label class="custom-control-label" for="chkIndicator{{ $ind->ind_id }}">
                              {{ $ind->ind_detail }}
                            </label>
                          @endif
                        </div>
                      </li>
                    @endforeach
                  @else
                    <li class="list-group-item">
                      <b>ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</b>
                    </li>
                  @endif
                </div>
              @endforeach
            </div> --}}

            {{-- <div class="accordion basic-accordion" id="div-type-office" role="tablist">
              <div class="row">
                @foreach ($office_type as $type)
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header" role="tab" id="heading{{$type->office_type_id}}">
                        <h6 class="mb-0">
                          <a data-toggle="collapse" href="#div-type{{$type->office_type_id}}" aria-expanded="false" aria-labelledby="heading{{$type->office_type_id}}" aria-controls="div-type{{$type->office_type_id}}">
                            <i class="card-icon fa fa-home"></i>
                            <u>ตัวชี้วัด{{$type->office_type_name}} พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</u>
                          </a>
                        </h6>
                      </div>
                      
                      <div id="div-type{{$type->office_type_id}}" class="collapse" role="tabpanel" data-parent="#div-type-office">
                        <div class="card-body">
                          <ul class="list-group">
                            @php
                              $indicators = DB::table('indicator')
                                ->where([
                                  'type_id'=>$type->office_type_id,
                                  'ind_year'=>$year, 
                                  'ind_round'=>$round
                                ])->get(); 
                            @endphp

                            @if(count($indicators) > 0)
                              @foreach ($indicators as $i=>$ind)
                                <li class="list-group-item">
                                  <div class="custom-control custom-checkbox">
                                    @if ($ind->num_02 != '#')
                                      <input type="checkbox" id="chkIndicator{{ $ind->ind_id }}" name="chkIndicator[]" value="{{ $ind->ind_id }}"  class="custom-control-input">
                                    
                                      <label class="custom-control-label" for="chkIndicator{{ $ind->ind_id }}">
                                        {{ $ind->ind_detail }}
                                      </label>
                                    @else
                                      <label class="col-form-label py-0" for="chkIndicator{{ $ind->ind_id }}">
                                        {{ $ind->ind_detail }}
                                      </label>
                                    @endif
                                  </div>
                                </li>
                              @endforeach
                            @else
                              <li class="list-group-item">
                                <b>ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</b>
                              </li>
                            @endif
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div> --}}

          </div>

          <div class="card-footer text-center">
            {{-- <input type="hidden" name="round" value="{{ $round }}"> --}}
            <button type="button" onclick="insertEvaluator()" class="btn btn-lg btn-success btnSave">
              <i class="far fa-save"></i> บันทึก
            </button>

            <button type="reset" class="btn btn-lg btn-danger">
              <i class="fa fa-undo"></i> ยกเลิก
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
  
@endsection

@section('js')
<script type="text/javascript"> 
  $('select[name="ddlGroup"]').change(function(e){
    // console.log(e.target.value);
    var group = $(this).val();
    var select_office = $('select[name="ddlOffice"] > option').not(':first');

    if(group==1 || group==4 || group==5 || group==6) {
      $('.tab-basic, .tab-content-basic').show();
    } else {
      $('.tab-basic, .tab-content-basic').hide();
    }
    
    $.each(select_office, function (i, val) {
      var office_type = select_office.eq(i).attr('office_type');
      console.log($(this).val());
      if(group==1 || group==4 || group==5 || group==6) {
        if( (office_type == 1) ) {
          select_office.eq(i).show();
        } else {
          select_office.eq(i).hide();
        }
      }

      else if(group==2 || group==3) {
        if( (office_type != 1) ) {
          select_office.eq(i).show();
        } else {
          select_office.eq(i).hide();
        }
      }

    });
    $('select[name="ddlOffice"] > option:first').prop('selected',true);
  });

  function insertEvaluator() { 
    $.ajax({
      type: "POST",
      url: "{{ url('setting/user-insert') }}",
      data: $('#frmInsert').serialize(),
      
      success: function (response) {
        console.log(response);
        alert(response.msg)
        if(response.validate > 0){
          $('.txtUsername').focus();
        }
      },
      error: function (err) {
        console.log(err.responseJSON);
      }
    });
  }

  function Validate(name, tagname){
    if(tagname == 'SELECT') {
      // console.log($('select[name="'+name+'"]').val());
      if($('select[name="'+name+'"]').val() == null) { // || $('select[name="'+name+'"]').val() == 0
        $('select[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('select[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    } 
    else if(tagname == 'INPUT') {
      // console.log(tagname);
      if($('input[name="'+name+'"]').val() == '') {
        $('input[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('input[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    }
  }

  function ValidateClick(){
    if($('.txtUsername').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtUsername').css('border-color','red');
      return false;
    }
    if($('.txtPassword').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtPassword').css('border-color','red');
      return false;
    }
    if($('.txtName').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtName').css('border-color','red');
      return false;
    }

    if($('.ddlGroup').val()==null){
      $('.btnSave').attr('disabled',true);
      $('.ddlGroup').css('border-color','red');
      return false;
    }
    if($('.ddlOffice').val()==null){
      $('.btnSave').attr('disabled',true);
      $('.ddlOffice').css('border-color','red');
      return false;
    }
  }
</script>
@endsection