@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')
<div class="row">
  <div class="col-12">
    <h1 class="form-inline">
      หน้าแรก
    {{-- <select class="form-control ml-2 ddlYear font-weight-bold-" onchange="getYear(this.value,$('.ddlRound').val())")">
        <option value="0">เลือกปี พ.ศ.</option>
        @foreach ($indicator_year_view as $y)
        <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
            ตัวชี้วัด พ.ศ.{{$y->ind_year}}
        </option>
        @endforeach
    </select>

    <select class="form-control ml-2 ddlRound" onchange="getYear($('.ddlYear').val(),this.value)">
        <option value="0">เลือกรอบการประเมิน</option>
        @foreach ($round_evaluate_view as $round_evaluate)
        <option value="{{ $round_evaluate->id }}" {{$round==$round_evaluate->id ?'selected' :''}}>{{ $round_evaluate->round_name }}</option>
        @endforeach
    </select> --}}
    </h1>
  </div>
</div>


<div class="row">
  <div class="col-md-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="text-center-">
          {{-- {{$data_user_view->office_name}} --}}
        </h4>

        <div class="row">
          <div class="col-md-12 text-center">
            <span> </span>
          </div>

          <div class="box- col-md-6">
            <div id="score_group1" class="gauge"></div>
          </div>

          <div class="col-md-6">
            <div id="score_group2" class="gauge"></div>
          </div>
        </div>

        <div class="row text-center d-flex justify-content-center">
            
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4>
            
        </h4>

        <div class="row">
          <div class="col-md-12 text-center">
            <span>
              
            </span>
          </div>
          <div class="col-md-6">
            <div id="score_group3" class="gauge"></div>
          </div>
          <div class="col-md-6">
            <div id="score_group4" class="gauge"></div>
          </div>
        </div>

        <div class="row text-center d-flex justify-content-center">
          {{-- <button type="button" class="btn btn-primary mt-4" id="gauge_refresh">Refresh Gauges</button> --}}
        </div>
      </div>
    </div>
  </div> 

</div>
@endsection

@section('js')
<script src="{{ url('pagin/paginathing.js') }}"></script>
<script>

  $('.acc-admin').paginathing({
    perPage: 10,
    insertAfter: '.am',
    pageNumbers: true
  });
  $('.acc-user').paginathing({
    perPage: 10,
    limitPagination: 4,
    insertAfter: '.us',
    pageNumbers: true
  });

</script>
@endsection
