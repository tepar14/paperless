<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link" style="display:block;">
        <div class="user-wrapper">
          <!-- <div class="profile-image">
            <img src="{{ url('img/icon.png') }}" class="rounded-circle">
          </div> -->
          <div class="text-wrapper text-center" style="width: 100%; margin-left: 0px;">
            <p class="profile-name" style="text-overflow: ellipsis; overflow: hidden;" title="">
              {{-- @if(Auth::User()->name!=null)
                {{ Auth::User()->name }}
              @else
              
              @endif --}}
            </p>

            {{-- <div style="text-overflow: ellipsis; overflow: hidden;">
              <small class="designation text-muted">
                {{ 'ประเภท : $data_user_view->position_type_name' }}
              </small>
            </div>

            <div style="text-overflow: ellipsis; overflow: hidden;">
              <small class="designation text-muted">
                {{ '$data_user_view->position_rank_name' }}
              </small>
            </div>

            <div style="text-overflow: ellipsis; overflow: hidden;">

              <small class="designation text-muted">
                  {{ '$data_user_view->position_name' }}
              </small>
              <span class="status-indicator online"></span>
            </div> --}}

          </div>
        </div>

        <button disabled onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-danger btn-block mt-1" style="background-color:#ff0000;">
          <i class="mdi mdi-power"></i> ออกจากระบบ
        </button>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ url('#') }}" aria-expanded="true" title="หน้าแรก">
        <i class="menu-icon mdi mdi-24px mdi-home"></i>
        <span class="menu-title">หน้าแรก</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ url('document-all') }}">
        <i class="menu-icon mdi mdi-24px mdi-console"></i>
        <span class="menu-title" title="ฟอร์มเอกสาร">ฟอร์มเอกสาร</span>
      </a>
    </li>

    {{-- @for ($i=1; $i<=1; $i++)
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#report-dropdown-{{ $i }}" aria-expanded="false" aria-controls="report-dropdown">
          <i class="menu-icon mdi mdi-24px mdi-file"></i>

          <span class="menu-title">
            Product
          </span>

          <i class="menu-arrow"></i>
        </a>

        <div class="collapse" id="report-dropdown-{{ $i }}">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('product1-'.$i) }}">product 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('product2-'.$i) }}">product 2</a>
            </li>
          </ul>
        </div>
      </li>
    @endfor --}}

    {{-- @php
      $arr_permission = explode(',',$data_user_view->permission_group);
    @endphp

    @if((string)in_array('111', $arr_permission))
  
      <li class="nav-item">
        <a class="ind nav-link" href="{{url('indicator')}}">
          <i class="menu-icon mdi mdi-24px mdi-file-chart"></i>
          <span class="menu-title" title="รายงานตัวชี้วัด">โครงการ/กิจกรรม</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{url('score')}}">
          <i class="menu-icon mdi mdi-24px mdi-format-list-numbers"></i>
          <span class="menu-title" title="ตรวจสอบคะแนน">ตรวจสอบคะแนน</span>
        </a>
      </li>
    @endif

    <li class="nav-item">
      <a class="nav-link" 
        data-toggle="collapse" href="#setting-dropdown" aria-expanded="false" aria-controls="setting-dropdown">
        <i class="menu-icon mdi mdi-24px mdi-settings-outline"></i>
        <span class="menu-title">ตั้งค่า</span>
        <i class="menu-arrow"></i>
      </a>

      <div class="collapse
        @if( Request::is('setting/user') || Request::is('setting/user-create') )
          {{ 'show' }}
        @endif

        @if(!empty($user->id))
          {{Request::is('setting/'.$user->id.'/user-edit') ?'show':''}}
        @endif" id="setting-dropdown">
        
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link
              @if( Request::is('setting/user') || Request::is('setting/user-create') )
                {{ 'active' }}
              @endif

              @if(!empty($user->id))
                {{ Request::is('setting/'.$user->id.'/user-edit') ?'active':'' }}
              @endif" href="{{ url('setting/user') }}">
              ตั้งค่าผู้ใช้งาน
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ url('setting/') }}">ตั้งค่า 2</a>
          </li>
        </ul>
      </div>
    </li> --}}

    {{-- <li class="nav-item">
      <a class="nav-link" href="{{url('developer')}}" target="blank_">
        <i class="menu-icon mdi mdi-24px mdi-console"></i>
        <span class="menu-title" title="สำหรับนักพัฒนาระบบ">สำหรับนักพัฒนาระบบ</span>
      </a>
    </li> --}}
  </ul>
</nav>
