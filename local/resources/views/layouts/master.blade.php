<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title') | {{ Config::get('app.name') }}</title>
  <link rel="shortcut icon" href="{{ url('').'/'.Config::get('app.icon') }}" />

  <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/puse-icons-feather/feather.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/fontawesome-5.8.1/css/all.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.addons.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/vendors/icheck/skins/all.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendors/lightgallery/css/lightgallery.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/shared/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/demo_1/style.css') }}">


  <style media="screen">
    /*---------- input ----------*/
    input[type=number]::-webkit-inner-spin-button {
      display: none;
      /* -webkit-appearance: none; */
    }

    /* ตาราง ฟอร์มเอกสาร */
    table {
      width: 100%;
      border-collapse: collapse;
    }

    .tbl- {
      width: 100%;
      border-collapse: collapse;
    }
    .tbl- th, .tbl- td {
      padding: 5px;
      border: 1px solid black;
    }

    .tbl-indicator thead>tr>th {
      border:;
      background: #006ee4;
      color:#fff;
      vertical-align: middle;
      font-size: 0.875rem;
      line-height: 1.35;
      padding: 18px 15px;
    }
    .tbl-indicator td{
      /* vertical-align: top; */
      font-size: 0.850rem;
      line-height: 1.35;
      padding: .45rem;
    }
  </style>
  @yield('css')
</head>

<body class="sidebar-fixed sidebar-icon-only">
  <div class="container-scroller">
    @include('layouts.navbar')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.sidebar')
      <div class="main-panel">
        <div class="content-wrapper">
          @yield('content')
        </div>
        @include('layouts.footer')
      </div>
    </div>
  </div>
</body>

<script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('assets/vendors/js/vendor.bundle.addons.js') }}"></script>

<script src="{{ asset('assets/vendors/lightgallery/js/lightgallery-all.min.js') }}"></script>

<script src="{{ asset('assets/js/demo_1/dashboard.js') }}"></script>
<script src="{{ asset('assets/js/demo_1/dashboard_2.js') }}"></script>
<script src="{{ asset('assets/js/shared/data-table.js') }}"></script>
<script src="{{ asset('assets/js/shared/off-canvas.js') }}"></script>
{{-- <script src="{{ asset('assets/js/shared/hoverable-collapse.js') }}"></script> --}}
<script src="{{ asset('assets/js/shared/misc.js') }}"></script>
<script src="{{ asset('assets/js/shared/settings.js') }}"></script>
<script src="{{ asset('assets/js/shared/iCheck.js') }}"></script>
<script src="{{ asset('assets/js/shared/alerts.js') }}"></script>
<script src="{{ asset('assets/js/shared/avgrund.js') }}"></script>
<script src="{{ asset('assets/js/shared/modal-demo.js') }}"></script>
<script src="{{ asset('assets/js/shared/todolist.js') }}"></script>
<script src="{{ asset('assets/js/shared/file-upload.js') }}"></script>
<script src="{{ asset('assets/js/shared/typeahead.js') }}"></script>
<script src="{{ asset('assets/js/shared/select2.js') }}"></script>
<script src="{{ asset('assets/js/shared/tooltips.js') }}"></script>
<script src="{{ asset('assets/js/shared/popover.js') }}"></script>
<script src="{{ asset('assets/js/shared/owl-carousel.js') }}"></script>

<script src="{{ asset('assets/js/shared/dropify.js') }}"></script>
<script src="{{ asset('assets/js/shared/light-gallery.js') }}"></script>


<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(function () {
    // alert($('span.notification').text())
    // realTime();
  });
  // $(window).load(realTime);
  // function realTime() {
  //   $.ajax({
  //       type:'post',
  //       url:'',
  //       data:{
  //           '_token':$('input[name=_token]').val(),
  //       },
  //       success: function (data) {
          
  //           console.log(data);

  //           if(data.num_notify > 0){
              
  //             $('span.notification').text(data.num_notify).show();
  //             $('a.notification').attr('style','display:flex !important');
  //           } else{
              
  //             $('span.notification').text(data.num_notify).hide();
  //             $('a.notification').attr('style','display:none !important');
  //           }
  //         let timeout = setTimeout(realTime, 3000);

  //         if(data.status_notify == 1 && data.status_accept == 1){
  //           clearTimeout(timeout);
  //         }
          
  //       },
  //   });
  // }
  
  $.fn.replaceClass = function (pFromClass, pToClass) {
    return this.removeClass(pFromClass).addClass(pToClass);
  };
  
  $('[data-tooltip="tooltip"]').tooltip();
  var arr_month = [
    'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
    'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
  ];

  $(function () {
    var thaiDate = new Date();
    var dd = thaiDate.getDate();
    var mm = arr_month[thaiDate.getMonth()];
    var yyyy = new String(thaiDate.getFullYear()+543).substr(2,4);
    $('#dateThai').html(dd+' '+mm+' '+yyyy);

    setInterval(function(){
      var Now = new Date();

      var h = Now.getHours();
      var m = new String(Now.getMinutes());
      if(m.length==1){
        m='0'+m;
      }
      var s = new String(Now.getSeconds());
      if(s.length==1){
        s='0'+s;
      }
      $('#diffTime').html(h +':'+ m +':'+ s);
      // $('#diffTime').html(dd +' '+ mm +' '+ yyyy +'&nbsp;&nbsp;&nbsp;'+ h +':'+ m +':'+ s);
    },1000);
  });
</script>

@yield('js')
</body>
</html>
