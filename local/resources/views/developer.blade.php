@extends('layouts.master')

@section('title', 'สำหรับนักพัฒนาระบบ')
@section('css')
  <style>

  </style>
@stop
@section('content')
<div class="row"> 
  <div class="col-12">
		<h2 class="form-inline">
      สำหรับนักพัฒนาระบบ

      <button type="button" class="ml-2 btn btn-outline-danger" onclick="DeleteData({{ Auth::user()->id }})">
        ห้ามกด !
      </button>
    </h2>
  </div>
</div>

<div class="row">
	<div class="col-md-12">
    <div class="card grid-margin">
      <div class="card-body">
        <h4 class="font-weight-bold">
            <u>
              กลุ่มผู้ใช้งาน (user_group)
            </u>
        </h4>
        <table class="tbl-indicator table-bordered">
          <thead class="text-center">
            <tr>
              <th width="%">รหัส</th>
              <th width="%">ชื่อกลุ่ม</th>
              <th width="%">น้ำหนักส่วนที่ 1</th>
              <th width="%">น้ำหนักส่วนที่ 2</th>
              <th width="%">หน้าที่/ความหมาย</th>
            </tr>
          </thead>
          <tbody>
            @php
              $user_group = DB::table('user_group')
                ->join('total_weight', 'total_weight.id', '=', 'user_group.total_weight_id')
                ->orderByRaw('user_group.id ASC')
              ->get();
            @endphp

            @foreach ($user_group as $i=>$group)
              <tr>
                <td class="text-center"> {{ $group->user_group_id }} </td>

                <td>{{ $group->user_group_name }}</td>
                
                <td class="text-center">{{ $group->total_weight1 }}</td>
                <td class="text-center">{{ $group->total_weight2 }}</td>

                <td>{{ $group->comment }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
        
      </div>
    </div>
        
    <div class="card grid-margin">
      <div class="card-body">    
        <h4 class="font-weight-bold">
          <u>
            สิทธิ์การใช้งาน
          </u>
        </h4>
        <table class="tbl-indicator table-bordered">
          <thead class="text-center">
            <tr>
              <th width="%">รหัส</th>
              <th width="%">ชื่อสิทธิ์</th>
            </tr>
          </thead>
          <tbody>
            @php
              $permissions = DB::table('permission')
              ->get();
            @endphp

            @foreach ($permissions as $i=>$permission)
              <tr>
                <td class="text-center"> {{ $permission->code }} </td>

                <td>{{ $permission->name }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
        
    <div class="card grid-margin">
      <div class="card-body">   
        
        <h4 class="font-weight-bold">
          <u>
            สำนัก/กอง (office)
          </u>
        </h4>
        <table class="tbl-indicator table-bordered">
          <thead class="text-center">
            <tr>
              <th width="%">รหัส</th>
              <th width="%">ชื่อกอง</th>
            </tr>
          </thead>
          <tbody>
            @php
              $offices = DB::table('office')
                // ->orderByRaw('user_group.id ASC')
              ->get();
            @endphp

            @foreach ($offices as $i=>$office)
              <tr>
                <td class="text-center"> {{ $office->office_code }} </td>

                <td>{{ $office->office_name }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>

	</div>
</div>
@endsection

@section('js')
<script>

  function DeleteData(emp_id) {
    if(confirm('ต้องการลบข้อมูลรายงานของคุณหรือไม่ ?')) { //console.log(1);
      $.ajax({
        type: "get",
        url: "{{ url('DeleteData') }}",
        data: {emp_id: emp_id},
        success: function (res) {
          alert(res.msg);
          console.log(res);
        }, 
        error: function(err) {
          console.log(err.responseJSON);
        }
      });
    }
  }
</script>
@endsection
