@extends('layouts.master')

@section('title', 'แก้ไขข้อมูลตัวชี้วัด')
@section('css')
  <style media="screen">
    .btnDeleteChoice{
      cursor: pointer;
    }
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-12">
      <h1 class="form-inline">แก้ไขข้อมูลตัวชี้วัด
      </h1>
    </div>

  </div>

  <div class="row"> 
    <div class="col-md-12">

      <form action="{{ url('admin/indicator').'/'.$indicator->ind_id.'/update' }}" method="post" enctype="multipart/form-data"> 
      {{-- <form id="frmInsert" method="post"> --}}
        @csrf
        <div class="card">
          <div class="card-body pb-3">

            <div class="accordion basic-accordion" id="accordion" role="tablist">
              <div class="card">
                <div class="card-header" role="tab">
                  <h6 class="mb-0">
                    <a data-toggle="collapse" href="#div-indicator" aria-expanded="true" aria-controls="div-indicator">
                      <i class="card-icon mdi mdi-checkbox-marked-circle-outline"></i><u>ข้อมูลตัวชี้วัด</u>
                    </a>
                  </h6>
                </div>

                <div id="div-indicator" class="collapse show" role="tabpanel" data-parent="#accordion">
                  <div class="card-body">
                    
                      <div class="row">
                        <div class="col-md-4 form-group">
                          <label>ประเภทหน่วยงาน</label>
                          <select name="ddlTypeOffice" class="form-control font-weight-bold- ddlTypeOffice" >
                            <option value="0">เลือกประเภทหน่วยงาน</option>
                            @foreach ($office_type as $type)
                              <option value="{{ $type->office_type_id }}" {{$indicator->type_id==$type->office_type_id ?'selected' :''}}>
                                ตัวชี้วัด {{ $type->office_type_name }}
                              </option>
                            @endforeach
                          </select>
                        </div>
          
                        <div class="col-md-4 form-group">
                          <label>ปี พ.ศ.</label>
                          <select name="ddlYear" class="form-control ddlYear" >
                            <option value="0">เลือกปี พ.ศ.</option>
                            @foreach ($ind_year as $y)
                              <option value="{{$y->ind_year}}" {{ $y->ind_year == $indicator->ind_year ? 'selected':''}}>
                                ตัวชี้วัด พ.ศ.{{$y->ind_year}}
                              </option>
                            @endforeach
                          </select>
                        </div>
          
                        <div class="col-md-4 form-group">
                          <label>รอบการประเมิน</label>
                          <select name="ddlRound" class="form-control ddlRound">
                            <option value="0">เลือกรอบการประเมิน</option>
                            <option value="1" {{ $indicator->ind_round==1 ?'selected' :'' }}>การประเมินรอบที่ 1</option>
                            <option value="2" {{ $indicator->ind_round==2 ?'selected' :'' }}>การประเมินรอบที่ 2</option>
                          </select>
                        </div>
                      </div>
          
                      <div class="row">
                        <div class="col-md-8 form-group">
                          <label>ตัวชี้วัด</label>
                          <input type="text" value="{{ $indicator->ind_detail }}" name="txtDetail" class="form-control">
                        </div>

                        <div class="col-md-4 form-group">
                          <label>คำอธิบายตัวชี้วัด</label>
                          <input type="file" name="txtDescFile" class="file-upload-default txtDescFile">
                          <div class="input-group col-xs-12">
                            <input type="text" value="{{ $indicator->ind_file }}" class="form-control file-upload-info file-name txtDescFileName" placeholder="ชื่อไฟล์" readonly>
                            <span class="input-group-append">
                              <button type="button" onclick="window.open('{{url('local/public/file').'/'.$indicator->ind_file}}','_blank')" class="btn btn-outline-success {{$indicator->ind_file!=null?'':'d-none'}}" title="คำอธิบาย">
                                <i class="fa fa-file-pdf"></i>
                              </button>

                              <button type="button" class="file-upload-browse btn btn-outline-primary" title="แนบไฟล์">
                                <i class="fa fa-paperclip"></i>
                              </button>
                            </span>
                          </div>
                          <!-- <div class="mt-2 {{$indicator->ind_file!=null?'':'d-none'}}">
                            <a href="{{url('local/public/file').'/'.$indicator->ind_file}}" target="_blank" class="btn btn-sm btn-inverse-primary btn-rounded btn-block" title="คำอธิบาย">
                              <i class="fa fa-file-pdf"></i> ดาวน์โหลด
                            </a>
                          </div> -->
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 1</label>
                          <input type="number" value="{{ $indicator->ind_weight1 }}" name="txtWeight1" class="form-control txtWeight1" {{$indicator->ind_round==1 ?'' :'readonly'}}>
                        </div>

                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 2 (1)</label>
                          <input type="number" value="{{ $indicator->ind_weight21 }}" name="txtWeight21" class="form-control txtWeight21" {{$indicator->ind_round==1 ?'readonly' :''}}>
                        </div>

                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 2 (2)</label>
                          <input type="number" value="{{ $indicator->ind_weight22 }}" name="txtWeight22" class="form-control txtWeight22" {{$indicator->ind_round==1 ?'readonly' :''}}>
                        </div>

                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 2 (3)</label>
                          <input value="{{ $indicator->ind_weight23 }}" name="txtWeight23" class="form-control txtWeight23" {{$indicator->ind_round==1 ?'readonly' :''}}>
                        </div>
                        
                        <div class="col-md-2 form-group">
                          <label>
                            num_1 
                            <!-- <br>Ex1. ข้อ1, 1.1, 1.2 ... num_1 => 1 
                            <br>Ex2. ข้อ2, 2.1, 2.2 ... num_1 => 2 -->
                          </label>
                          <input type="number" value="{{ $indicator->num_01 }}" name="txtNum1" class="form-control">
                        </div>

                        <div class="col-md-2 form-group">
                          <label>
                            num_2 
                            <!-- <br>Ex1.
                            <br>ข้อ 1 ใหญ่ (แบบมี Choice) => 0 
                            <br>ข้อ 1 ใหญ่ (แบบไม่มี Choice) => # 
                            <br>Ex2.
                            <br>ข้อ 1.1 => 1, ข้อ 1.2 => 2 
                            <br>ข้อ 2.1 => 1, ข้อ 2.2 => 2 -->
                          </label>
                          <input value="{{ $indicator->num_02 }}" name="txtNum2" class="form-control">
                        </div>
                      </div>    

                      <div class="row">
                        <div class="col-md-3 form-group">
                          <label>ประเภทคำตอบ</label>
                          <select name="ddlTypeAnswer" class="form-control ddlTypeAnswer">
                            <option value="0">เลือกประเภทคำตอบ</option>
                            <option value="value" {{$indicator->ind_answer=='value' ?'selected' :''}}>Value</option>
                            <option value="checkbox" {{$indicator->ind_answer=='checkbox' ?'selected' :''}}>Checkbox</option>
                          </select>
                        </div>

                        <div class="col-md-3 form-group">
                          <label>สูตร</label>
                          <select name="ddlFormula" class="form-control ddlFormula">
                            <option value="0">เลือกสูตร</option>
                            <option value="(Number($('.txtAnswer0').val()) * 100) / Number($('.txtAnswer1').val())" {{$indicator->formula=="(Number($('.txtAnswer0').val()) * 100) / Number($('.txtAnswer1').val())" ?'selected' :''}}>
                              (ข้อ1 x 100) / ข้อ2
                            </option>
                            <option value="(Number($('.txtAnswer1').val()) * 100) / Number($('.txtAnswer2').val())" {{$indicator->formula=="(Number($('.txtAnswer1').val()) * 100) / Number($('.txtAnswer2').val())" ?'selected' :''}}>
                              (ข้อ2 x 100) / ข้อ3
                            </option>
                            <option value="Number($('.txtAnswer0').val()) / Number($('.txtAnswer1').val())" {{$indicator->formula=="Number($('.txtAnswer0').val()) / Number($('.txtAnswer1').val())" ?'selected' :''}}>
                              ข้อ1 / ข้อ2
                            </option>
                            
                            <option value="-">ไม่มี</option>
                          </select>
                        </div>
                        
                        <div class="col-md-3 form-group">
                          <label>กลุ่มคะแนน</label>
                          <select name="ddlScoreGroup" class="form-control ddlScoreGroup">
                            <option value="0">กลุ่มคะแนน</option>
                            <option value="1" {{$indicator->score_group=='1' ?'selected' :''}}>ประเมินประสิทธิผล (มิติภายนอก)</option>
                            <option value="2" {{$indicator->score_group=='2' ?'selected' :''}}>การประเมินคุณภาพ (มิติภายนอก)</option>
                            <option value="3" {{$indicator->score_group=='3' ?'selected' :''}}>ประเมินประสิทธิภาพ (มิติภายใน)</option>
                            <option value="4" {{$indicator->score_group=='4' ?'selected' :''}}>การพัฒนาองค์การ (มิติภายใน)</option>
                          </select>
                        </div>
          
                        <div class="col-md-3 form-group">
                          <label>วันที่สิ้นสุดการรายงาน</label>
                          <input type="date" value="{{ $indicator->date_limit }}" name="txtDateLimit" class="form-control">
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
              
              <div class="card">
                <div class="card-header" role="tab">
                  <h6 class="mb-0">
                    <a data-toggle="collapse" href="#div-indicator-choice" aria-expanded="true" aria-controls="div-indicator-choice">
                      <i class="card-icon mdi mdi-checkbox-multiple-marked-circle-outline"></i>
                      <u>ข้อย่อยตัวชี้วัด</u>
                    </a>
                  </h6>
                </div>

                <div id="div-indicator-choice" class="collapse show" role="tabpanel" data-parent="#accordion">
                  <div class="card-body">
                    <div class="div-tbl-choice table-responsive">
                      
                      {{-- <h5 class="text-danger remark" style="display:none;">กรุณากรอกข้อมูลให้ครบทุกช่อง</h5> --}}
                      <button type="button" class="btn btn-lg btn-primary w-100" onclick="AddChoice()">
                        <i class="fa fa-plus"></i> เพิ่มข้อย่อยตัวชี้วัด
                      </button>

                      <table class="tbl-choice table-bordered mt-3" width="100%">
                        @foreach ($choices as $i=>$choice)
                          <tr class="tr-choice tr-choice-{{ $choice->ic_id }}">
                            <td width="10%" class="text-center td-num">
                              {{ ($i+1) }} 
                              {{-- {{ $choice->ic_id }} --}}
                            </td>

                            <td width="10%">
                              <input value="{{ $choice->ic_section }}" name="txtSection[{{ $choice->ic_id }}]" class="form-control txtSection" placeholder="Section">
                            </td>

                            <td width="70%"> 
                              <div class="input-group col-xs-12">
                                <input value="{{ $choice->ic_detail }}" name="txtChoice[{{ $choice->ic_id }}]" class="form-control txtChoice">
                                <span class="input-group-append">
                                  <button type="button" onclick="DeleteChoice({{ $choice->ic_id }})" class="btnDeleteChoice btn btn-outline-danger" title="ลบข้อมูล">
                                    <i class="fa fa-trash-alt"></i>
                                  </button>
                                </span>
                              </div>
                            </td>
                          </tr>
                        @endforeach
                      </table>
                           
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="card-footer text-center">
            <button type="" class="btn btn-lg btn-success btn-add">
              <i class="far fa-save"></i> บันทึก
            </button>

            <button type="reset" class="btn btn-lg btn-danger">
              <i class="fa fa-undo"></i> ยกเลิก
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
  
@endsection

@section('js')
<script type="text/javascript"> 
  @if(\Session::has('success'))
    alert('{{ \Session::get('success') }}');
  @endif 

  function AddChoice(){ 
    // alert($('.tr-choice').length)
    let answer;
    answer  ='<tr class="tr-choice tr-choice-new">';
    answer +=   '<td width="10%" class="text-center td-num"></td>';
    answer +=   '<td width="10%">';
    answer +=     '<input name="txtSection[]" class="form-control txtSection" placeholder="Section">';
    answer +=   '</td>';
    answer +=   '<td width="70%">';
    answer +=     '<div class="input-group col-xs-12">';
    answer +=       '<input type="text" name="txtChoice[]" class="form-control txtChoice" placeholder="ข้อย่อย">';
    answer +=       '<span class="input-group-append">';
    answer +=         '<button type="button" class="btnDeleteChoice btn btn-outline-danger" title="">';
    answer +=           '<i class="fa fa-trash-alt"></i>';
    answer +=         '</button>';
    answer +=       '</span>';
    answer +=     '</div>';
    answer +=   '</td>';
    answer +='</tr>';

    $('.tbl-choice').append(answer);
    Runnumber();
  }

  function Runnumber() { 
    let row_choice = $('.tr-choice').length;
    for(let i=0; i<row_choice; i++){
      let num_new = 'n'+(i+1);
      $('.td-num').eq(i).text((i+1));
      $('.tr-choice-new').find($('.txtSection')).eq(i).attr({name:'txtSection['+num_new+']'});
      $('.tr-choice-new').find($('.txtChoice')).eq(i).attr({name:'txtChoice['+num_new+']', onfocusout:''});
    }
  }

  //ลบข้อย่อย
  $(".tbl-choice").on('click', ".btnDeleteChoice", function (e) {
    $(this).closest('tr.tr-choice-new').remove();
    Runnumber();
  });

  function DeleteChoice(choice_id) { 
    if (confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
      $.ajax({
        type: "get",
        url: "{{ url('admin/indicator_choice/delete') }}",
        data: {choice_id: choice_id},
        success: function (msg) {
          alert(msg);
          $('tr.tr-choice-'+choice_id).remove();
          Runnumber();
          // location.reload();
        }
      });
    }
  }

  $('.ddlRound').change(function (e) { 
    e.preventDefault();
    let round = $(this).val();
    if(round == 1){
      $('.txtWeight1').attr({'readonly':false});

      $('.txtWeight21').attr({'readonly':true, 'value':''});
      $('.txtWeight22').attr({'readonly':true, 'value':''});
      $('.txtWeight23').attr({'readonly':true, 'value':''});
    }else{
      $('.txtWeight21').attr({'readonly':false});
      $('.txtWeight22').attr({'readonly':false});
      $('.txtWeight23').attr({'readonly':false});
      
      $('.txtWeight1').attr({'readonly':true, 'value':''});
    }
    
  });

  function ValidateChoice(name){
    if($('input[name="'+name+'"]').val() == '') {
      $('input[name="'+name+'"]').css('border-color', '#FF0000');
      $('.btn-add').attr('disabled',true);
    }else{
      $('input[name="'+name+'"]').css('border-color', '#2eb82e');
      $('.btn-add').attr('disabled',false)
    }
  }

  $('.btn-add').click(function() {
    //ดักค่าบับันทึกข้อหลัก


    //ดักค่าบันทึกข้อย่อย
    let row_choice = $('.tr-choice').length;
    // if(row_choice == 0){
    //   swal({type: 'info',title: 'ยังไม่ได้เพิ่มข้อมูลใบ Invoice',
    //   showConfirmButton: false});
    //   $(this).attr('disabled',true);
    // }
    // for(i=0; i < row_choice; i++){
    //   if($('.txtChoice').eq(i).val()==''){
    //     $(this).attr('disabled',true);
    //     $('.txtChoice').eq(i).css('border-color','red');
    //   }
    // }
  });
</script>
@endsection