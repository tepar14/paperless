@extends('layouts.master')

@section('title', 'ข้อมูลผู้ใช้งาน')
@section('css')
  <style type="">
    
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-8">
      <h2 class="form-inline">ข้อมูลผู้ใช้งาน
      </h2>
    </div>

    <div class="col-md-4 text-right">
      <a href="{{url("admin/create/user")}}" class="btn btn-lg btn-success">
        <i class="fa fa-plus"></i> เพิ่มข้อมูลผู้ใช้งาน
      </a>
    </div>

  </div>

  <div class="row"> 
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <ul class="nav nav-tabs tab-basic" role="tablist">
            @foreach ($office_type as $type)
              <li class="nav-item">
                <a class="nav-link {{$type->office_type_id==1 ?'active' :''}}" id="tab-head{{$type->office_type_id}}" data-toggle="tab" href="#tab-type{{$type->office_type_id}}" role="tab" aria-controls="tab-type{{$type->office_type_id}}" aria-selected="{{$type->office_type_id==2 ?'true' :'false'}}">
                  {{$type->office_type_name}}
                </a>
              </li>
            @endforeach
          </ul>
          
          <div class="tab-content tab-content-basic">
            @foreach ($office_type as $type)
              <div class="tab-pane fade {{$type->office_type_id==1 ?'show active' :''}}" id="tab-type{{$type->office_type_id}}" role="tabpanel" aria-labelledby="tab-head{{$type->office_type_id}}">
                <table class="tbl-indicator table-bordered">
                  <thead class="text-center">
                    <tr>
                      <th width="5%">ลำดับ</th>
                      <th width="%">ชื่อ</th>
                      <th width="%">ตำแหน่ง</th>
                      <th width="%">หน้าที่</th>
                      <th width="10%">แก้ไข / ลบ</th>
                    </tr>
                  </thead>  
                  <tbody>
                    @php
                      $users = DB::table('users')
                        ->select('*')
                        ->leftjoin('office', 'office.office_id', '=', 'users.office_id')
                        ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
                        ->leftjoin('user_group', 'user_group.id', '=', 'users.user_group_id')
                        ->where([
                          ['office_type.office_type_id', '=', $type->office_type_id]
                        ])
                        ->orderByRaw('office_type.office_type_id ASC, users.office_id ASC, user_group.id ASC')
                      ->get();
                    @endphp
                    
                    @if(count($users) > 0)
                      @foreach ($users as $i=>$user)
                        <tr class="bg-secondary">
                          <td class="text-center">{{$i+1}}</td>

                          <td>
                              <span>{{ $user->name }}</span>
                          </td>

                          <td>
                            @if ($user->position == null)
                              <span class="w-100 text-center">-</span>
                            @else
                              <span>{{ $user->position }}</span>
                            @endif
                          </td>

                          <td>
                            <span>{{ $user->user_group_name }}</span>
                          </td>

                          <td class="text-center"> 
                            {{-- <a href="{{ url('admin/indicator').'/'.$ind->ind_id.'/edit' }}" class="btn btn-icons btn-warning" title="แก้ไข">
                              <i class="fa fa-edit"></i>
                            </a>

                            <button type="button" onclick="DeleterIndicator({{ $ind->ind_id }})" class="btn btn-icons btn-danger">
                              <i class="fa fa-trash-alt"></i>
                            </button> --}}
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <tr>
                        <th colspan="3" class="text-center"></th>
                      </tr>
                    @endif
                  </tbody>
                </table> 
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script type="text/javascript"> 
  @if(\Session::has('success'))
    alert('{{ \Session::get('success') }}');
  @endif 
   
  function getYear(year,round) {
    if(year==0 && round==0){ //console.log(1);
      window.location.href='{{url('admin/indicator')}}';
    } else if(year!=0 && round!=0){ //console.log(2);
      // window.location.href='{{url('admin/indicator')}}'+'/'+year+'/'+round;
      window.location.href= '{{url("admin")}}'+'/'+year+'/'+round+'/indicator';
    }
  }

  function EditIndicator(ind_id){ 
    if($('.txtDescFile'+ind_id).val()==''){
      var file = $('.txtDescFileName'+ind_id).val();
    }else{ 
      var file = $('.txtDescFile'+ind_id)[0].files[0];
    }

    var form_data = new FormData();
    form_data.append('_token', '{{csrf_token()}}');
    form_data.append('txtIndID', ind_id);
    form_data.append('txtDetail', $('.txtDetail'+ind_id).val());
    form_data.append('txtDate', $('.txtDate'+ind_id).val());
    form_data.append('txtDescFile', file);
    $.ajax({
      url: '{{ url('indicator/edit') }}',
      type: 'POST',
      data: form_data,
      dataType:'JSON',
      contentType: false,
      processData: false,
      success: function (res) { console.log(res);
        alert(res.msg);
        if($('.txtDescFile'+ind_id).val()!=''){
          $('.desc-downloaded'+ind_id).removeClass('d-none').empty().append(res.download);
        }
        $('.txtDescFileName'+ind_id).val(res.file_name);
        // location.reload();
      }
    });
  }

  function DeleterIndicator(indicator_id) { 
    if (confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
      $.ajax({
        type: "get",
        url: "{{ url('admin/indicator/delete') }}",
        data: {indicator_id: indicator_id},
        success: function (msg) {
          alert(msg);
          location.reload();
        }
      });
    }
  }
</script>
@endsection