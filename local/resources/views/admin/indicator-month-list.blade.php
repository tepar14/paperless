@extends('layouts.master')

@section('title', 'รายงานตัวชี้วัด')
@section('css')
  <style type="">
    
  </style>
@stop
@section('content')
<div class="row">
    <div class="col-12">
      <h2 class="form-inline">เลือกประเภทหน่วยงาน
        <select class="form-control ml-2 ddlYear font-weight-bold-" onchange="getYear(this.value,$('.ddlRound').val())")">
          <option value="0">เลือกปี พ.ศ.</option>
          @foreach ($ind_year as $y)
            <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
              ตัวชี้วัด พ.ศ.{{$y->ind_year}}
            </option>
          @endforeach
        </select>
    
        <select name="" class="form-control ml-2 ddlRound" onchange="getYear($('.ddlYear').val(),this.value)">
          <option value="0">เลือกรอบการประเมิน</option>
          <option value="1" {{$round==1 ?'selected' :''}}>การประเมินรอบที่ 1</option>
          <option value="2" {{$round==2 ?'selected' :''}}>การประเมินรอบที่ 2</option>
        </select>
      </h2>
    </div>
  </div>

  <div class="row"> 
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <ul class="nav nav-tabs tab-basic" role="tablist">
            @foreach ($office_type as $type)
              <li class="nav-item">
                <a class="nav-link {{$type->office_type_id==2 ?'active' :''}}" id="tab-head{{$type->office_type_id}}" data-toggle="tab" href="#tab-type{{$type->office_type_id}}" role="tab" aria-controls="tab-type{{$type->office_type_id}}" aria-selected="{{$type->office_type_id==2 ?'true' :'false'}}">
                  ตัวชี้วัด{{$type->office_type_name}}
                </a>
              </li>
            @endforeach
          </ul>
          
          <div class="tab-content tab-content-basic">
            @foreach ($office_type as $type)
              <div class="tab-pane fade {{$type->office_type_id==2 ?'show active' :''}}" id="tab-type{{$type->office_type_id}}" role="tabpanel" aria-labelledby="tab-head{{$type->office_type_id}}">
                <table class="tbl-indicator table-bordered">
                  <thead class="text-center">
                    <tr>
                      <th width="5%">ลำดับ</th>
                      <th width="%">ตัวชี้วัด</th>
                    </tr>
                  </thead>  
                  <tbody>
                    @php
                      $indicators = DB::table('indicator')
                        ->where(['type_id'=>$type->office_type_id, 'ind_year'=>$year, 'ind_round'=>$round])
                      ->get();
                    @endphp
                    @if(count($indicators) > 0)
                      @foreach ($indicators as $i=>$ind)
                        <tr class="bg-secondary">
                          <td class="text-center">{{$i+1}}</td>
                          <td>
                            @if ($ind->num_02=='#')
                              <span>{{$ind->ind_detail}}</span>
                            @else
                              <a href="{{ url('admin/indicator-month/'.$ind->ind_id) }}" target="">
                                {{$ind->ind_detail}}
                              </a>
                            @endif
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <tr>
                      <th colspan="2" class="text-center">ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}}</th>
                      </tr>
                    @endif
                  </tbody>
                </table> 
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script type="text/javascript">  
  function getYear(year,round) {
    if(year==0 && round==0){ //console.log(1);
      window.location.href='{{url('admin/indicator')}}';
    } else if(year!=0 && round!=0){ //console.log(2);
      // window.location.href='{{url('admin/indicator')}}'+'/'+year+'/'+round;
      window.location.href= '{{url("admin")}}'+'/'+year+'/'+round+'/indicator';
    }
  }

  function EditIndicator(ind_id){ 
    if($('.txtDescFile'+ind_id).val()==''){
      var file = $('.txtDescFileName'+ind_id).val();
    }else{ 
      var file = $('.txtDescFile'+ind_id)[0].files[0];
    }

    var form_data = new FormData();
    form_data.append('_token', '{{csrf_token()}}');
    form_data.append('txtIndID', ind_id);
    form_data.append('txtDetail', $('.txtDetail'+ind_id).val());
    form_data.append('txtDate', $('.txtDate'+ind_id).val());
    form_data.append('txtDescFile', file);
    $.ajax({
      url: '{{ url('indicator/edit') }}',
      type: 'POST',
      data: form_data,
      dataType:'JSON',
      contentType: false,
      processData: false,
      success: function (res) { console.log(res);
        alert(res.msg);
        if($('.txtDescFile'+ind_id).val()!=''){
          $('.desc-downloaded'+ind_id).removeClass('d-none').empty().append(res.download);
        }
        $('.txtDescFileName'+ind_id).val(res.file_name);
        // location.reload();
      }
    });
  }
</script>
@endsection