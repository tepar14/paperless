@extends('layouts.master')

@section('title', 'ผลคะแนนของหน่วยงานตามเขตตรวจราชการ')
@section('css')
  <style>
    
  </style>
@stop
@section('content')
<div class="row">
  <div class="col-md-9">
		<h2 class="form-inline">
      ผลคะแนนหน่วยงานผู้ตรวจราชการ
    </h2>
  </div>

  <div class="col-md-3 text-right">
    
    <button type="button" onclick="printScore({{$year}},{{$round}},{{$inspector_zone}})" class="btn btn-lg btn-primary">
      <i class="fa fa-print"></i> พิมพ์
    </button>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
		<h2 class="form-inline">
      <select class="form-control ml-2 ddlYear font-weight-bold-" onchange="getYear(this.value,$('.ddlRound').val(),{{$inspector_zone}})">
        <option value="0">เลือกปี พ.ศ.</option>
        @foreach ($indicator_year_view as $y)
          <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
            ตัวชี้วัด พ.ศ.{{$y->ind_year}}
          </option>
        @endforeach
      </select>

      <select name="" class="form-control ml-2 ddlRound" onchange="getYear($('.ddlYear').val(),this.value,{{$inspector_zone}})">
        <option value="0">เลือกรอบการประเมิน</option>
        @foreach ($round_evaluate_view as $round_evaluate)
          <option value="{{ $round_evaluate->id }}" {{$round==$round_evaluate->id ?'selected' :''}}>{{ $round_evaluate->round_name }}</option>
        @endforeach
      </select>
    </h2>
  </div>
</div>

<div class="row">
	<div class="col-12">
    <div class="card">
      <div class="card-body">
        
        <table class="tbl-indicator table-bordered">
          <thead class="text-center">
            <tr>
              <th width="%">ลำดับ</th>
              <th width="%">หน่วยงาน</th>
              <th width="%">คะแนนถ่วงน้ำหนักรวม</th>
            </tr>
          </thead>  
          <tbody>
            
            @php
            $total_score = [];
            $sum_score = 0;
            $avg_score = 0;
            
            if($round==1) {
              $total_score = DB::select("SELECT
                transaction_result.office_id,
                office.office_name,
                office_type.office_type_name,
                indicator.ind_round,
                indicator.ind_year,
                office.group_weight,
                SUM( (indicator.ind_weight1 * transaction_result.score) /100)  AS total_score
                FROM
                  transaction_result
                  INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                    AND transaction_result.round =?
                  INNER JOIN office ON office.office_id = transaction_result.office_id
                  INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                  
                WHERE indicator.ind_year =?
                  AND indicator.ind_round =?
                  AND office.inspector_zone = ?

                GROUP BY 
                  transaction_result.office_id
                ORDER BY
                  total_score DESC
              ", [$round, $year, $round, $inspector_zone]); 
            }

            if($round==2) {
              $value_1 = [];
              $value_2 = [];
              $value_3 = [];

              for($i=1;$i<=3;$i++) {
                if($i==1) {
                  $value_1 = DB::select("SELECT
                    transaction_result.office_id,
                    --CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                    office.office_name,
                    office_type.office_type_name,
                    indicator.ind_round,
                    indicator.ind_year,
                    office.group_weight,
                    SUM( (indicator.ind_weight21 * transaction_result.score) /100)  AS total_score
                    FROM
                      transaction_result
                      INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                        AND transaction_result.round =?
                      INNER JOIN office ON office.office_id = transaction_result.office_id
                      INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                      
                    WHERE indicator.ind_year =?
                      AND indicator.ind_round =?
                      AND office.inspector_zone =?
                      AND office.group_weight = $i
                    GROUP BY 
                      transaction_result.office_id
                    ORDER BY
                      total_score DESC
                  ", [$round, $year, $round, $inspector_zone]); 
                }

                else if($i==2) {
                  $value_2 = DB::select("SELECT
                    transaction_result.office_id,
                    office.office_name,
                    office_type.office_type_name,
                    indicator.ind_round,
                    indicator.ind_year,
                    office.group_weight,
                    SUM( (indicator.ind_weight22 * transaction_result.score) /100)  AS total_score
                    FROM
                      transaction_result
                      INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                        AND transaction_result.round =?
                      INNER JOIN office ON office.office_id = transaction_result.office_id
                      INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                      
                    WHERE indicator.ind_year =?
                      AND indicator.ind_round =?
                      AND office.inspector_zone = ?
                      AND office.group_weight = $i
                    GROUP BY 
                      transaction_result.office_id
                    ORDER BY
                      total_score DESC
                  ", [$round, $year, $round, $inspector_zone]); 
                }

                else if($i==3) {
                  $value_3 = DB::select("SELECT
                    transaction_result.office_id,
                    office.office_name,
                    office_type.office_type_name,
                    indicator.ind_round,
                    indicator.ind_year,
                    office.group_weight,
                    SUM( (indicator.ind_weight23 * transaction_result.score) /100)  AS total_score
                    FROM
                      transaction_result
                      INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                        AND transaction_result.round =?
                      INNER JOIN office ON office.office_id = transaction_result.office_id
                      INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                      
                      WHERE indicator.ind_year =?
                      AND indicator.ind_round =?
                      AND office.inspector_zone = ?
                      AND office.group_weight = $i
                    GROUP BY 
                      transaction_result.office_id
                    ORDER BY
                      total_score DESC
                  ", [$round, $year, $round, $inspector_zone]); 
                }
              }

              // echo "----".count($value_1)."<br>";
              if(count($value_1) > 0)
                foreach ($value_1 as $key => $total) {
                  array_push($total_score,$value_1[$key]);
                }

                if(count($value_2) > 0)
                  foreach ($value_2 as $key => $total) {
                    array_push($total_score,$value_2[$key]);
                  }
              // echo "----".count($value_3)."<br>";
              if(count($value_3) > 0)
                foreach ($value_3 as $key => $total) {
                  array_push($total_score,$value_3[$key]);
                  // echo $total->office_name."  ";
                  //   echo number_format($total->total_score,4,'.','')."<br>";
                } 
            }
            // }
            /////////////////////////////////////////////end office_type_id != 2 (office)////////////////////////////////////////

            @endphp

            @if(count($total_score) > 0)
              @foreach ($total_score as $i=>$total)
                @php
                  $sum_score += $total->total_score;
                @endphp

                <tr>
                  <td class="text-center"> {{ ($i+1) }} </td>
                  
                  <td>
                    {{ $total->office_name }}
                  </td>

                  <td>
                    {{ number_format($total->total_score,4,'.','') }}
                  </td>
                </tr>
              @endforeach
              <tr class="bg-secondary">
                <th colspan="2" class="text-center">รวม</th>

                <th>{{ number_format( ($sum_score / count($total_score)) ,4,'.','') }}</th>
              </tr>
            @else
              <tr>
                <th colspan="3" class="text-center">ยังไม่มีข้อมูลของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</th>
              </tr>
            @endif


          </tbody>
        </table> 
            
      </div>

  

    </div>
	</div>
</div>
@endsection

@section('js')
<script>
  
  function printScore (year,round,inspector_zone) { 
    window.open("{{ url('print/scoreinspector/') }}" +'/'+year+'/'+round+'/'+inspector_zone, "_blank");
  }
  
  function getYear(year,round,inspector_zone) {
    if(year==0 && round==0 && inspector_zone==0){ //console.log(1);
      window.location.href='{{url('admin/scoreinspector')}}';
    } else if(year!=0 && round!=0 && inspector_zone!=0){ //console.log(2);
      window.location.href= '{{url("admin")}}'+'/'+year+'/'+round+'/'+inspector_zone+'/scoreinspector';
    }
  }
</script>
@endsection
