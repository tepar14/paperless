<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Office;
use App\OfficeType;
use App\Indicator;
use App\IndicatorChoice;
use App\TransactionChoice;
use App\TransactionFile;
use App\TransactionResult;
use App\TransactionMonth;
use Auth; 
use DB;
use PDF; 
// use Illuminate\Support\Facades\Response;
// use Illuminate\Support\Facades\Request;

class AdminController extends Controller
{
    //!ตรวจสอบข้อมูลดิบรายเดือน

    public function getIndexMonth($ind_id){
      $office_type = OfficeType::where([['office_type_id','<>','1']])->get(); //dd($office_type[0]->Name_office_type);
      
      $indicators = Indicator::where(['ind_id'=>$ind_id])->orderBy('ind_id')->first(['ind_detail']);
      $choices = IndicatorChoice::where('ic_ind_id', $ind_id)->orderBy('ic_ind_id','asc')->get();
      $office = DB::select("SELECT
        office.office_id,
        office.office_name
        FROM office
        INNER JOIN transaction_month tm ON office.office_id = tm.office_id
        WHERE tm.ind_id =? 
        GROUP BY tm.office_id
      ", [$ind_id]);

      // $data_users =  DB::table('users')->where(['users.id'=>Auth::user()->id])
      //   ->join('office', 'office.office_id', '=', 'users.office_id')
      //   // ->select('office_type_id')
      // ->first();
      $transaction_month = DB::select("SELECT
        ic.ic_id,
        ic.ic_detail,
        tm.tm_id,
        tm.ind_id,
        tm.tm_answer1, tm.tm_answer2, tm.tm_answer3, tm.tm_answer4, tm.tm_answer5, tm.tm_answer6,
        tm.tm_answer7, tm.tm_answer8, tm.tm_answer9, tm.tm_answer10, tm.tm_answer11, tm.tm_answer12,
        tm.office_id,
        office.office_name
        FROM indicator_choice ic 
        LEFT JOIN transaction_month tm ON tm.ic_id = ic.ic_id
        LEFT JOIN office ON office.office_id = tm.office_id
        WHERE tm.ind_id =? 
      ", [$ind_id]);
      // return $transaction_month;
      return view('admin/indicator-month', compact('ind_id','office_type','indicators','choices','transaction_month','office'));
    }

    //! ผู้ประเมินผล
    //todo รายงานสรุปผลคะแนนของหน่วยงานตามเขตพื้นที่
    public function getReportTotalScoreZone() {
      $round = '2';
      $year = '2562'; //(date('Y')+543);
      $zone = '1';
      return view('admin/report/totalscore-zone', compact('year','round','zone'));
    }

    public function getReportTotalScoreZoneYear($year, $round, $zone) {

      return view('admin/report/totalscore-zone', compact('year','round','zone'));
    }

    public function printScoreZone($year, $round, $zone){ 
      $zone_evaluate = DB::table('zone_evaluate')
        ->select('*')
        ->where('id',$zone)
      ->first();

      //pdf
      set_time_limit(150);
      $pdf = PDF::loadView('admin/report/pdf-zone', compact('year', 'round', 'zone', 'zone_evaluate'));
      $pdf->setPaper('a4','portrait');
      //  $pdf->setPaper('a4','landscape');
      $fileName = 'ตารางสรุปคะแนนผลการประเมิน รอบการประเมินที่ '.$round.' พ.ศ.'.$year;
      return $pdf->stream($fileName.'.pdf');
    }

    //todo รายงานสรุปผลคะแนนของหน่วยงานตามผู้ตรวจการ
    public function getReportTotalScoreInspector() {
      //query ผู้ตรวจราชการ
      $inspector_all = DB::table('users')
        ->select('users.id','users.name','users.position')
        ->where('users.user_group_id',6)
        ->orderByRaw('users.id ASC')
      ->get();

      $round = '2';
      $year = '2562'; //(date('Y')+543);

      if(Auth::User()->user_group_id == '1' || Auth::User()->user_group_id == '4' || Auth::User()->user_group_id == '5'){
        $inspector_zone = $inspector_all[0]->id; //'424'
        $page = 'admin/report/totalscore-inspector';
      }else{
        $inspector_zone = Auth::User()->id;
        $page = 'admin/report/totalscore-byinspector';
      }

      return view($page, compact('year','round','inspector_zone', 'inspector_all'));
    }

    public function getReportTotalScoreInspectorYear($year, $round, $inspector_zone) {
      $inspector_all = DB::table('users')
        ->select('users.id','users.name','users.position')
        ->where('user_group_id',6)
      ->get();

      if(Auth::User()->user_group_id == '1' || Auth::User()->user_group_id == '4' || Auth::User()->user_group_id == '5'){
        $page = 'admin/report/totalscore-inspector';
      }else{
        $page = 'admin/report/totalscore-byinspector';
      }
      return view($page, compact('year','round','inspector_zone', 'inspector_all'));
    }

    public function printScoreInspector($year, $round, $inspector_zone){ 
      $inspector = DB::table('users')
        ->select('users.id','users.name','users.position')
        ->where('id',$inspector_zone)
      ->first();

      if(Auth::User()->user_group_id == '1' || Auth::User()->user_group_id == '4' || Auth::User()->user_group_id == '5'){
        $page = 'admin/report/pdf-inspector';
      }else{
        $page = 'admin/report/pdf-byinspector';
      }
      //pdf
      set_time_limit(150);
      $pdf = PDF::loadView($page, compact('year','round','inspector_zone', 'inspector'));
      $pdf->setPaper('a4','portrait');
      //  $pdf->setPaper('a4','landscape');
      $fileName = 'ตารางสรุปคะแนนผลการประเมิน รอบการประเมินที่ '.$round.' พ.ศ.'.$year;
      return $pdf->stream($fileName.'.pdf');
    }

    //todo รายงานสรุปผลคะแนนของหน่วยงาน (Chart)
    public function getReportTotalScoreChart() {
      $round = '2';
      $year = '2562';

      return view('admin/report/totalscore-chart', compact('year','round') );
    }

    public function getReportTotalScoreChartYear($year, $round) {

      return view('admin/report/totalscore-chart', compact('year','round'));
    }

    //! ผู้ดูแลระบบ
    //todo เพิ่ม แก้ไข ลบ ข้อมูลตัวชี้วัด
    public function getIndicator() {
      $year = '2562'; //(date('Y')+543);
      $round = 1;
      
      $ind_year = Indicator::where(['ind_round'=>$round])
        ->groupBy('ind_year')
      ->get(['ind_year']); //query พ.ศ.ทั้งหมดของตัวชี้วัดทุกหน่วยงาน
      // $ind_month = Indicator::groupBy('ind_month')->get(['ind_month']); //query รอบเดือนทั้งหมดของตัวชี้วัด
      
      return view('admin/indicator-list', compact('ind_year','year','round'));
    }
    
    public function getIndicatorYear($year, $round) {
      $ind_year = Indicator::where(['ind_round'=>$round])
        ->groupBy('ind_year')
      ->get(['ind_year']);
      
      return view('admin/indicator-list', compact('ind_year','year','round'));
    }
    
    public function createIndicator() {
      $year = '2562'; //(date('Y')+543);
      $round = 1;
      // $ind_month = Indicator::groupBy('ind_month')->get(['ind_month']); //query รอบเดือนทั้งหมดของตัวชี้วัด
      return view('admin/indicator-create', compact('year','round'));
    }

    public function insertIndicator(Request $req) {
      // dd($req->all());

      //? Run num_01, num_02
      $maxnum_01 =  DB::table('indicator')
        ->where([
          'type_id'=>$req->ddlTypeOffice,
          'ind_year'=>$req->ddlYear,
          'ind_round'=>$req->ddlRound,
        ])
      ->max('num_01'); 
      if($maxnum_01 == null){
        $new_num_01 = 1;
      } else { 
        // $new = substr($maxnum_01,-6) + 1;
        $new_num_01 = $maxnum_01 + 1;
      } 

      if ($req->txtNum1 == NULL) {
        $num_01 = $new_num_01;
      } else {
        $num_01 = $req->txtNum1;
      }
      $maxnum_02 =  DB::table('indicator')
        ->where([
          'num_01'=>$num_01,
          'type_id'=>$req->ddlTypeOffice,
          'ind_year'=>$req->ddlYear,
          'ind_round'=>$req->ddlRound,
        ])
      ->max('num_02'); 
      if($maxnum_02 == null){
        $new_num_02 = 1;
      } else { 
        $new_num_02 = $maxnum_02 + 1;
      } 
      // dd($new_num_02);

      //? บันทึกตัวชี้วัด
      $indicator = new Indicator();
      $indicator->ind_detail = $req->txtDetail;
      $indicator->ind_answer = $req->ddlTypeAnswer;
      $indicator->formula = $req->ddlFormula;
      $indicator->ind_round = $req->ddlRound;
      $indicator->ind_year = $req->ddlYear;
      $indicator->type_id = $req->ddlTypeOffice;
      
      $indicator->score_group = $req->ddlScoreGroup;

      if ($req->txtNum1 == NULL) { 
        $indicator->num_01 = $new_num_01;
      } else { 
        $indicator->num_01 = $req->txtNum1;
      }

      if ($req->txtNum2 == NULL) {
        $indicator->num_02 = $new_num_02;
      } else { 
        $indicator->num_02 = $req->txtNum2;
      }

      if($req->ddlRound == 1){
        $indicator->ind_weight1 = $req->txtWeight1; 
      } else {
        $indicator->ind_weight21 = $req->txtWeight21;
        $indicator->ind_weight22 = $req->txtWeight22;
        $indicator->ind_weight23 = $req->txtWeight23;
      }

      if ($req->txtDateLimit == NULL) {
        $indicator->date_limit = date('Y').'-12-31';
      } else {
        $indicator->date_limit = $req->txtDateLimit;
      }
      
      if($req->hasFile('txtDescFile')) {
        $filename = 'Y'.$req->ddlYear.'R'.$req->ddlRound.'T'.$req->ddlTypeOffice.'_'.$req->file('txtDescFile')->getClientOriginalName();
        // $filename = 'desc'.$req->txtIndID.'_'.date('dmY').'_'.str_random(5) . '.' . $req->file('txtDescFile')->getClientOriginalExtension();
        // return $filename; 
        $req->file('txtDescFile')->move(public_path('file'), $filename);
        $indicator->ind_file = $filename;
      }
      $indicator->save();


      // บันทึกข้อย่อยตัวชี้วัด
      // if(count($req->txtChoice) > 0){
      if($req->txtChoice){
        foreach ($req->txtChoice as $key=>$value) {
          $choice = new IndicatorChoice();
          $choice->ic_section = $req->txtSection[$key];
          $choice->ic_detail = $value;
          $choice->ic_ind_id = $indicator->ind_id;
          $choice->round = $req->ddlRound;
          $choice->type_id = $req->ddlTypeOffice;

          $choice->save();
        }
        // return 'มี choice';
      }
      
      // return 'บันทึกข้อมูลสำเร็จ';
      return redirect('admin/indicator')->with('success','บันทึกข้อมูลสำเร็จ');
    }

    public function editIndicator($ind_id) { 
      // dd(55);
      $ind_year = Indicator::where([])
        ->groupBy('ind_year')
      ->get(['ind_year']);
      
      $office_type = OfficeType::where([['office_type_id','<>','1']])->get();
      
      $indicator =  DB::table('indicator')->where(['ind_id'=>$ind_id])
        // ->select('')
      ->first();
      $choices = DB::select("SELECT 
        indicator_choice.ic_id,
        indicator_choice.ic_detail,
        indicator_choice.ic_section,
        indicator_choice.round,
        indicator_choice.type_id
        FROM indicator_choice
        WHERE ic_ind_id=? 
        ORDER BY indicator_choice.ic_id
      ", [$ind_id]);

      return view('admin/indicator-edit', compact('ind_id','office_type','ind_year','indicator','choices'));
    }

    public function updateIndicator(Request $req, $ind_id) {
      // dd($req->txtWeight1);
      //? Run num_01, num_02
      $maxnum_01 =  DB::table('indicator')
        ->where([
          'type_id'=>$req->ddlTypeOffice,
          'ind_year'=>$req->ddlYear,
          'ind_round'=>$req->ddlRound,
        ])
      ->max('num_01'); 
      if($maxnum_01 == null){
        $new_num_01 = 1;
      } else { 
        // $new = substr($maxnum_01,-6) + 1;
        $new_num_01 = $maxnum_01 + 1;
      } 

      if ($req->txtNum1 == NULL) {
        $num_01 = $new_num_01;
      } else {
        $num_01 = $req->txtNum1;
      }
      $maxnum_02 =  DB::table('indicator')
        ->where([
          'num_01'=>$num_01,
          'type_id'=>$req->ddlTypeOffice,
          'ind_year'=>$req->ddlYear,
          'ind_round'=>$req->ddlRound,
        ])
      ->max('num_02'); 
      if($maxnum_02 == null){
        $new_num_02 = 1;
      } else { 
        $new_num_02 = $maxnum_02 + 1;
      }  

      // แก้ไขตัวชี้วัด
      $indicator = Indicator::find($ind_id);
      $indicator->ind_detail = $req->txtDetail;
      $indicator->ind_answer = $req->ddlTypeAnswer;
      $indicator->formula = $req->ddlFormula;
      $indicator->ind_round = $req->ddlRound;
      $indicator->ind_year = $req->ddlYear;
      $indicator->type_id = $req->ddlTypeOffice;
      $indicator->score_group = $req->ddlScoreGroup;

      if ($req->txtNum1 == NULL) { 
        $indicator->num_01 = $new_num_01;
      } else {
        $indicator->num_01 = $req->txtNum1;
      }

      if ($req->txtNum2 == NULL) {
        $indicator->num_02 = $new_num_02;
      } else {
        $indicator->num_02 = $req->txtNum2;
      }

      if($req->ddlRound == 1){
        $indicator->ind_weight1 = $req->txtWeight1; 
      } else {
        $indicator->ind_weight21 = $req->txtWeight21;
        $indicator->ind_weight22 = $req->txtWeight22;
        $indicator->ind_weight23 = $req->txtWeight23;
      }

      if ($req->txtDateLimit == NULL) {
        $indicator->date_limit = date('Y').'-12-31';
      } else {
        $indicator->date_limit = $req->txtDateLimit;
      }

      if($req->hasFile('txtDescFile')) {
        if(file_exists(public_path('file/'.$indicator->ind_file)) && $indicator->ind_file != NULL) {
          unlink(public_path('file/'.$indicator->ind_file)); //ถ้ามีการแนบไฟล์ใหม่ จะลบไฟล์เดิมทิ้ง
        }
        $filename = 'Y'.$req->ddlYear.'R'.$req->ddlRound.'T'.$req->ddlTypeOffice.'_'.$req->file('txtDescFile')->getClientOriginalName();
        $req->file('txtDescFile')->move(public_path('file'), $filename);
      } else {
        $filename = $req->txtDescFile;
      }

      $indicator->save();

      // แก้ไขข้อย่อยตัวชี้วัด
      if($req->txtChoice){
        // dd($req->txtSection);
        foreach ($req->txtChoice as $key=>$value) {
          
          $choice = IndicatorChoice::where(['ic_id'=>$key])->first();
          if($choice != null){
            $choice->ic_detail = $value;
            $choice->ic_section = $req->txtSection[$key];
            $choice->ic_ind_id = $indicator->ind_id;
            $choice->round = $req->ddlRound;
            $choice->type_id = $req->ddlTypeOffice;
            $choice->save();
          } else {
            // $choice_new = new IndicatorChoice();
            // $choice_new->ic_detail = $value;
            // $choice->ic_section = $req->txtSection[$key];
            // $choice_new->ic_ind_id = $indicator->ind_id;
            // $choice_new->round = $req->ddlRound;
            // $choice_new->type_id = $req->ddlTypeOffice;
            // $choice_new->save();
            DB::table('indicator_choice')->insert([
              [
                'ic_detail'=>$value, 'ic_section'=>$req->txtSection[$key], 'ic_ind_id'=>$indicator->ind_id, 
                'round'=>$req->ddlRound, 'type_id'=>$req->ddlTypeOffice
              ]
            ]);
          }

        }
      }

      // return 'แก้ไขสำเร็จ';
      return redirect()->back()->with('success','แก้ไขข้อมูลสำเร็จ');
    }

    public function deleteIndicator(Request $req) {
      $indicator = DB::table('indicator')->where('ind_id', $req->indicator_id)->delete();
      $choice = DB::table('indicator_choice')->where('ic_ind_id', $req->indicator_id)->delete();

      return 'ลบข้อมูลสำเร็จ';
    }

    public function deleteIndicatorChoice(Request $req) {
      // $choice = IndicatorChoice::find($req->choice_id)->delete();
      $choice = DB::table('indicator_choice')->where('ic_id', $req->choice_id)->delete();
      return 'ลบข้อมูลสำเร็จ';
    }

    //todo เพิ่ม แก้ไข ลบ ข้อมูลผู้ใช้งาน
    public function getUser() {
      $office_type= DB::table('office_type')
        ->select('office_type_id','office_type_name')
      ->get();

      return view('admin/user-list', compact('office_type'));
    }

    public function createUser() {
      $office_type= DB::table('office_type')
        ->select('office_type_id','office_type_name')
      ->get();

      return view('admin/user-create', compact('office_type'));
    }

    public function insertUser(Request $req) {
      dd($req->all());

      //? Run num_01, num_02
      $maxnum_01 =  DB::table('indicator')
        ->where([
          'type_id'=>$req->ddlTypeOffice,
          'ind_year'=>$req->ddlYear,
          'ind_round'=>$req->ddlRound,
        ])
      ->max('num_01'); 
      if($maxnum_01 == null){
        $new_num_01 = 1;
      } else { 
        // $new = substr($maxnum_01,-6) + 1;
        $new_num_01 = $maxnum_01 + 1;
      } 

      if ($req->txtNum1 == NULL) {
        $num_01 = $new_num_01;
      } else {
        $num_01 = $req->txtNum1;
      }
      $maxnum_02 =  DB::table('indicator')
        ->where([
          'num_01'=>$num_01,
          'type_id'=>$req->ddlTypeOffice,
          'ind_year'=>$req->ddlYear,
          'ind_round'=>$req->ddlRound,
        ])
      ->max('num_02'); 
      if($maxnum_02 == null){
        $new_num_02 = 1;
      } else { 
        $new_num_02 = $maxnum_02 + 1;
      } 
      // dd($new_num_02);

      //? บันทึกตัวชี้วัด
      $user = new User();
      $user->username = $req->txtUser;
      $user->name = $req->txtName;
      $user->position = $req->txtPosition;
      $user->tel = $req->txtTel;
      $user->email = $req->txtEmail;
      $user->office_id = $req->ddlOffice;
      $user->user_group_id = $req->ddlGroup;

      // if ($req->txtNum1 == NULL) { 
      //   $user->num_01 = $new_num_01;
      // } else { 
      //   $user->num_01 = $req->txtNum1;
      // }
     
      $indicator->save();
      
      // return 'บันทึกข้อมูลสำเร็จ';
      return redirect('admin/indicator')->with('success','บันทึกข้อมูลสำเร็จ');
    }

    //todo ตรวจสอบคะแนนตามประเภทหน่วยงาน
    public function checkTotalScore() {
      
      $year = '2562';
      $round = 2;

      $office_type = DB::table('office_type')
        ->where([['office_type_id','<>','1']])
      ->get(['office_type_id','office_type_name']);

      $ind_year = Indicator::groupBy('ind_year')->get(['ind_year']);
      
      // $group_weight = Office::groupBy('group_weight')->get(['group_weight']);
      return view('admin/totalscore', compact('office_type','ind_year','year','round'));
    }

    public function checkTotalScoreYear($year, $round) {

      $office_type = DB::table('office_type')
        ->where([['office_type_id','<>','1']])
      ->get(['office_type_id','office_type_name']);
      $ind_year = Indicator::groupBy('ind_year')->get(['ind_year']);
      return view('admin/totalscore', compact('office_type','ind_year','year','round'));
    }



    //ตั้งค่าผู้ตรวจประเมินผลรายงาน (กำหนดรายคนในส่วนกลาง)
    public function getEvaluator(Request $req) {
      $evaluators =  DB::table('users')->whereIn('users.user_group_id',[1,4])
        ->leftjoin('office', 'office.office_id', '=', 'users.office_id')
        ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
        ->select('users.id','users.name','user_group_id','users.evaluate_group','office.office_name')
      ->get(); 
      
      $year = '2562';
      $round =1;
      $ind_year = Indicator::groupBy('ind_year')->get(['ind_year']);
      $office_type = OfficeType::where([['office_type_id','<>','1']])->get(); 
      // $evaluator->pluck('office_type_id')->toArray(); 

      // return $evaluators;
      return view('admin/evaluator-list', compact('evaluators','office_type','ind_year','year','round'));
    }

    public function getEvaluatorYear($year, $round) {
      $evaluators =  DB::table('users')->whereIn('users.user_group_id',[1,4])
        ->leftjoin('office', 'office.office_id', '=', 'users.office_id')
        ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
        ->select('users.id','users.name','user_group_id','users.evaluate_group','office.office_name')
      ->get(); 
      
      
      $ind_year = Indicator::groupBy('ind_year')->get(['ind_year']);
      $office_type = OfficeType::where([['office_type_id','<>','1']])->get(); 
      // $evaluator->pluck('office_type_id')->toArray(); 

      // return $evaluators;
      return view('admin/evaluator-list', compact('evaluators','office_type','ind_year','year','round'));
    }

    public function editEvalute(Request $req) {
      $evaluate_group = implode(',', $req->chkIndicator);
      // return $req->round;

      $user = User::find($req->user_id);
      if($req->round == 1){
        $user->evaluate_group = $evaluate_group;
      } else {
        $user->evaluate_group2 = $evaluate_group;
      }
      $user->save();

      return 'บันทึกสำเร็จ';
    }

    


    //ตั้งค่าผู้ตรวจประเมินผลรายงาน (กำหนดรายหน่วยงานนส่วนกลาง)1
  /*  public function getEvaluator(Request $req) {
      $evaluators =  DB::table('office')->whereIn('office.office_type_id',[1])
        ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
        ->select('office.office_id','office.office_name')
      ->get(); 
      
      $year = '2562';
      $round =1;
      $ind_year = Indicator::groupBy('ind_year')->get(['ind_year']);
      $office_type = OfficeType::where([['office_type_id','<>','1']])->get(); 
      // $evaluator->pluck('office_type_id')->toArray(); 
      // return $evaluators;
      return view('admin/evaluator-list2', compact('evaluators','office_type','ind_year','year','round'));
    }

    public function getEvaluatorYear($year, $round) {
      $evaluators =  DB::table('office')->whereIn('office.office_type_id',[1])
        ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
        ->select('office.office_id','office.office_name')
      ->get();  
      
      $ind_year = Indicator::groupBy('ind_year')->get(['ind_year']);
      $office_type = OfficeType::where([['office_type_id','<>','1']])->get(); 
      return view('admin/evaluator-list2', compact('evaluators','office_type','ind_year','year','round'));
    }

    public function editEvalute(Request $req) {
      foreach ($req->chkIndicator as $key => $item) {
        $indicator = Indicator::find($item);
        $indicator->office_eval = $req->office_id;
        $indicator->save();
      }

      return 'บันทึกสำเร็จ';
    } 
  */  
}
