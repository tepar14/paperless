<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Visitor;

use Carbon\Carbon;
use DataTables;
use Validator; 
use Auth; 
use Hash;
use DB;

class DocumentController extends Controller
{
  
  public function getDocument(Request $req) {

    if ($req->ajax()) {
      $documents = DB::table('users')
        // ->whereIn('users.user_group_id', [1,2,3,4,5,6])
        // ->leftjoin('user_group', 'user_group.id', '=', 'users.user_group_id')
        // ->leftjoin('office', 'office.office_id', '=', 'users.office_id')
        // ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
        ->select(
          '*'
        )
        // ->orderByRaw('user_group.num_order ASC')
      ->get(); 
    }

    return Datatables::of($documents)->toJson();
  }
  
  // public function getDocument(Request $req) {
  //   return view('paper/paper-list' );
  // }
  
}
