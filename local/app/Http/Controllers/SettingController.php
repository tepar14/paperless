<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Users;

use Carbon\Carbon;
use DataTables;
use Validator; 
use Auth; 
use Hash;
use DB;

class SettingController extends Controller
{
  public function getUser(Request $req) {

    if ($req->ajax()) {
      $residents = DB::table('users')
        // ->whereIn('users.user_group_id', [1,2,3,4,5,6])
        // ->leftjoin('user_group', 'user_group.id', '=', 'users.user_group_id')
        // ->leftjoin('office', 'office.office_id', '=', 'users.office_id')
        // ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
        ->select(
         '*'
        )
        // ->orderByRaw('user_group.num_order ASC')
      ->get(); 
    }

    return Datatables::of($residents)->toJson();
  }

  public function createUser(Request $req) {

    return view('setting/user-create' );
  }

  public function insertUser(Request $req) {
    $validate = Validator::make($req->all(), [
      'txtCardID' => 'unique:resident,card_id',
      'txtTel' => 'unique:resident,tel',
      'txtAddress' => 'unique:resident,address',
    ], [ // 'txtFile.image'=>'ต้องเป็นไฟล์รูปภาพเท่านั้น',required|image
      'txtCardID.unique' => 'มีรหัสประจำตัวประชาชนนี้ในระบบแล้ว',
      'txtTel.unique' => 'มีเบอร์โทรศัพท์นี้ในระบบแล้ว',
      'txtAddress.unique' => 'มีที่อยู่นี้ในระบบแล้ว',
    ]);

    if($validate->passes()) {
      $resident = new User();
      
      $resident->name = $req->txtName;
      $resident->card_id = $req->txtCardID;
      $resident->address = $req->txtAddress;
      $resident->tel = $req->txtTel;

      $resident->save();

      $error = count($validate->errors());
      $msg = 'บันทึกข้อมูลสำเร็จ';
    } 
    else {
      $error = count($validate->errors());
      $msg = $validate->errors()->all();
    }
    
    return response()->json([
      'error'=> $error,
      'msg' => $msg,
    ]);
  }

  public function editUser(Request $req, $id) {
    $user = DB::table('users')->where(['users.id'=>$id])
      ->select(
        '*'
      )
    ->first(); ;

    return view('setting/user-edit', compact('user'));
  }

  public function updateUser(Request $req, $id) {
    $validate = Validator::make($req->all(), [
      'txtCardID' => 'unique:resident,card_id,'.$id.',id',
      'txtTel' => 'unique:resident,tel,'.$id.',id',
      'txtAddress' => 'unique:resident,address,'.$id.',id',
    ], [
      'txtCardID.unique' => 'มีรหัสประจำตัวประชาชนนี้ในระบบแล้ว',
      'txtTel.unique' => 'มีเบอร์โทรศัพท์นี้ในระบบแล้ว',
      'txtAddress.unique' => 'มีที่อยู่นี้ในระบบแล้ว',
    ]);

    if($validate->passes()) {
      $resident = User::find($id);

      $resident->name = $req->txtName;
      $resident->card_id= $req->txtCardID;
      $resident->address = $req->txtAddress;
      $resident->tel = $req->txtTel;

      $resident->save();

      $error = count($validate->errors());
      $msg = 'แก้ไขข้อมูลสำเร็จ';
    } 
    else {
      $error = count($validate->errors());
      $msg = $validate->errors()->all();
    }
    
    return response()->json([
      'error' => $error,
      'msg' => $msg,
    ]);
  }

  public function deleteUser($id) {

    $resident_delete = User::destroy($id);
    
    if($resident_delete) {
      $msg = 'ลบข้อมูลสำเร็จ';
    } else {
      $msg = 'ลบข้อมูลไม่สำเร็จ';
    }

    return response()->json([
      'msg'=>$msg
    ]);

  }
  
}
