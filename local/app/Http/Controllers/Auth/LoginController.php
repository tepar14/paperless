<?php

namespace App\Http\Controllers\Auth;
 
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request; //addnew
class LoginController extends Controller 
{
    use AuthenticatesUsers; 

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    // protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    //ADDNEW
    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $req)
    { // $req->get($this->username())
        $field = filter_var($req->txtUser, FILTER_VALIDATE_EMAIL) ?$this->username() :'username';

        return [
            // $field => $req->get($this->username()),
            $field => $req->txtUser,
            'password' => $req->txtPass, 
        ];
    }
}
