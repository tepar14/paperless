<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();

        // $gate->define('IsAdmin', function($user){
        //     return $user->ID_type_users == '1';
        // });

        // $gate->define('IsSale', function($user){
        //     return $user->ID_type_users == '2';
        // });

        // $gate->define('IsAccount', function($user){
        //     return $user->ID_type_users == '5';
        // });

        // $gate->define('IsUser', function($user){
        //     return $user->ID_type_users == null;
        // });
        
    }
}
