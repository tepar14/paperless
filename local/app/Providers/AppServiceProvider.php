<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\users;
use DB;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('*', function ($view) {
          if (Auth::check()) {
            
            $data_user_view = DB::table('users')->where(['users.id'=>Auth::user()->id])
                ->select(
                  '*'
                )
            ->first();

            $view->with([
              'data_user_view'=>$data_user_view
            ]);
            //'office_type_view'=>$office_type_view,'indicator_year_view'=>$indicator_year_view,'admins_'=>$admins_,'users_'=>$users_,
          }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
