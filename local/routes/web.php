<?php
Auth::routes();
 
Route::get('/', function () { return view('auth.login'); })->middleware('checklogin');
Route::get('/developer-', function () { return view('developer'); })->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
//!
//?
//* 
//todo demo
Route::get('document-all', function () { return view('document/document-all'); });
Route::get('document-list', function () { return view('document/document-list'); })->middleware('auth');
Route::get('get-document-list', 'DocumentController@getDocument')->middleware('auth');


//todo สำหรับ Reception
//? ข้อมูลลูกบ้าน
Route::get('setting/user', function () { return view('setting/user-list'); })->middleware('auth');
Route::get('setting/get-user', 'SettingController@getUser')->middleware('auth');
Route::get('setting/user-create', 'SettingController@createUser')->middleware('auth');
Route::post('setting/user-insert', 'SettingController@insertUser')->middleware('auth');
Route::get('setting/{id}/user-edit', 'SettingController@editUser')->middleware('auth');
Route::post('setting/{id}/user-update', 'SettingController@updateUser')->middleware('auth');
Route::post('setting/{id}/user-delete', 'SettingController@deleteUser')->middleware('auth');

//? ข้อมูลผู้เข้าพบ